import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {
    CalendarPage, BookingRoomPage, DetailRoomPage, MainRoomPage,
    DisplayWeb, DetailChatPage, ListChatPage, PresencePage, HomePage,
    SplashScreen, ProfilePage, AttendanceHistory, ListPodcastPage,
    DetailPodcastPage, MyTaskPage, LoginPage, ForgetPassword, LeavePage,
    MedicalBenefitPage, MyRoomPage, PodcastPage } from '../pages';
import { colors } from '../utils/colors';
import Icon from 'react-native-vector-icons/Ionicons';
import { View } from 'react-native';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import LottieView from 'lottie-react-native';
import { chatIcon, clockIcon, homeIcon, profileIcon, taskIcon } from '../assets';

const MaterialBottom = createBottomTabNavigator();
// const MaterialBottom = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

const HomeStack = () => {
    return (
        <Stack.Navigator
            initialRouteName="SplashScreen"
            screenOptions={{
                headerStyle: {
                    backgroundColor: colors.merah
                    // height: 100,
                },
                headerTintColor: '#ffffff',
                headerTitleStyle: {
                    fontSize: 20
                }
            }}
        >
            <Stack.Screen
                name="SplashScreen"
                component={SplashScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="MyTaskPage"
                component={MyTaskPage}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="Login"
                component={LoginPage}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="ForgetPassword"
                component={ForgetPassword}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="Home"
                component={BotomTab}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="AttendanceHistory"
                component={AttendanceHistory}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="PodcastPage"
                component={PodcastPage}
                options={{
                    headerShown: false
                }}/>
            <Stack.Screen
                name="PodcastStack"
                component={PodcastStack}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="DetailChatPage"
                component={DetailChatPage}
                options={{
                    headerShown: true
                }}
            />
            <Stack.Screen
                name="DisplayWeb"
                component={DisplayWeb}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="ListPodcastPage"
                component={ListPodcastPage}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="DetailPodcastPage"
                component={DetailPodcastPage}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="MainRoomPage"
                component={MainRoomPage}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="DetailRoomPage"
                component={DetailRoomPage}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="BookingRoomPage"
                component={BookingRoomPage}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="CalendarPage"
                component={CalendarPage}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="MedicalBenefitPage"
                component={MedicalBenefitPage}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="LeavePage"
                component={LeavePage}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="MyRoomPage"
                component={MyRoomPage}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
    );
};


const PodcastStack = ({ }) => {
    return (
        <Stack.Navigator initialRouteName="ListPodcastPage">
            <Stack.Screen
                name="ListPodcastPage"
                component={ListPodcastPage}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="DetailPodcastPage"
                component={DetailPodcastPage}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
    );
};

const ChatStack = ({ }) => {
    return (
        <Stack.Navigator initialRouteName="ListPodcastPage">
            <Stack.Screen
                name="ListChatPage"
                component={ListChatPage}
                options={{
                    headerShown: false
                }}
            />

        </Stack.Navigator>
    );
};


// const ReviewStack = ({ navigation }) => {
//     return (
//         <Stack.Navigator
//             initialRouteName="UserReviewPage"
//             screenOptions={{
//                 headerStyle: {
//                     backgroundColor: colors.background,
//                     height: 100,
//                 },
//                 headerTintColor: '#ffffff',
//                 headerTitleStyle: {
//                     fontSize: 20,
//                 }
//             }}
//         >
//             <Stack.Screen
//                 name="UserReviewPage"
//                 component={UserReviewPage}
//                 options={{
//                     title: "Your Reviews",
//                     headerTitleStyle: { alignSelf: 'flex-end' },
//                     headerShown: true,
//                     headerLeft: () => (
//                         <Icon.Button
//                             name='chevron-back-outline'
//                             size={20}
//                             backgroundColor={colors.temaStatusBar}
//                             onPress={() => navigation.navigate("HomePage")}
//                         >
//                             <Text style={{ color: '#FFFFFF', fontSize: 12 }}>
//                                 Home
//                             </Text>
//                         </Icon.Button>
//                     )
//                 }}
//             />
//             <Stack.Screen
//                 name="AllUserReviewPage"
//                 component={AllUserReviewPage}
//                 options={{
//                     title: "All Reviews",
//                     headerTitleStyle: { alignSelf: 'flex-end' },
//                     headerShown: true,
//                     headerLeft: () => (
//                         <Icon.Button
//                             name='chevron-back-outline'
//                             size={20}
//                             backgroundColor={colors.temaStatusBar}
//                             onPress={() => navigation.navigate("UserReviewPage")}
//                         >
//                             <Text style={{ color: '#FFFFFF', fontSize: 12 }}>
//                                 Your Review
//                             </Text>
//                         </Icon.Button>
//                     )
//                 }}
//             />
//         </Stack.Navigator>
//     )
// }


// const BotomTab = () => {
//     return (
//         <Tabs.Navigator
//             screenOptions={({ route }) => ({
//                 tabBarIcon: ({ focused, color, size }) => {
//                     let filePath;

//                     switch (route.name) {
//                         case "HomePage":
//                             filePath = homeIcon;
//                             break;
//                         case "PresencePage":
//                             filePath = clockIcon;
//                             break;
//                         case "ProfilePage":
//                             filePath = profileIcon;
//                             break;
//                         default:
//                             iconName = focused
//                                 ? "ios-information-circle"
//                                 : "ios-information-circle-outline";
//                     }
//                     // return <Ionicons name={iconName} size={size} color={color} />;
//                     return <LottieView source={filePath} autoPlay={focused} />;
//                 },
//             })}
//         >
//             <Tabs.Screen name="HomePage" component={HomePage} />
//             <Tabs.Screen name="PresencePage" component={PresencePage} />
//             <Tabs.Screen name="ProfilePage" component={ProfilePage} />
//         </Tabs.Navigator>
//     )
// }





const BotomTab = () => {
    return (
        <MaterialBottom.Navigator
            // shifting={false}
            // initialRouteName="HomePage"
            // style={{ position: 'absolute' }}
            // barStyle={{
            //     backgroundColor: colors.temaCard,
            //     borderWidth: .5,
            //     borderColor: colors.temaCard,
            //     // height: 82,
            //     justifyContent: 'center',
            //     paddingTop: 10,

            // }}
            initialRouteName="HomePage"
            tabBarOptions={{
                activeTintColor: colors.temaUtama,
                // activeBackgroundColor:'red',
                // tabStyle:{paddingTop:15},
                showLabel: false,
                style: { height: 60, backgroundColor: '#e0e0e0'}

            }}
            backBehavior="initialRoute"
            lazy="false"
        // activeColor={colors.temaCard}
        // inactiveColor='black'
        // activeBackgroundColor='red'
        >
            <MaterialBottom.Screen name="HomePage" component={HomePage}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ color }) => (
                        <View style={{ width: 30, height: 30, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                            {color == colors.temaUtama ?
                                <Icon name={'ios-home'} size={30} color={colors.merah} />
                                : <Icon name={'ios-home-outline'} size={30} color="black" />
                            }
                        </View>
                    )
                }}
            />
            <MaterialBottom.Screen name="MyTaskPage" component={MyTaskPage}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ color }) => (
                        <View style={{ width: 30, height: 30, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                            {color == colors.temaUtama ?
                                <Icon name={'ios-list-circle'} size={30} color={colors.merah} />
                                : <Icon name={'ios-list-circle-outline'} size={30} color="black" />
                            }
                        </View>
                    )
                }}
            />
            <MaterialBottom.Screen name="PresencePage" component={PresencePage}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ color }) => (
                        <View style={{ position: 'absolute', alignItems: 'center', elevation: 20 }}>
                            <View style={styles.button}>
                                <TouchableHighlight underlayColor="yellow">
                                    {color == colors.temaUtama ?
                                        <Icon name={'ios-stopwatch'} size={60} color={colors.merah} />
                                        : <Icon name={'ios-stopwatch-outline'} size={60} color="black" />
                                    }
                                </TouchableHighlight>
                            </View>
                        </View>
                    )
                }}
            />
            <MaterialBottom.Screen name="ChatStack" component={ChatStack}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ color }) => (
                        <View style={{ width: 30, height: 30, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                            {color == colors.temaUtama ?
                                <Icon name={'ios-chatbox-ellipses'} size={30} color={colors.merah} />
                                : <Icon name={'ios-chatbox-ellipses-outline'} size={30} color="black" />
                            }
                        </View>
                    )
                }}
            />
            <MaterialBottom.Screen name="ProfilePage" component={ProfilePage}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ color }) => (
                        <View style={{ width: 30, height: 30, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                            {color == colors.temaUtama ?
                                <Icon name={'ios-person'} size={30} color={colors.merah} />
                                : <Icon name={'ios-person-outline'} size={30} color="black" />
                            }
                        </View>
                    )
                }}
            />
        </MaterialBottom.Navigator>
    );
};

export default class Router extends Component {
    render() {
        return (
            <NavigationContainer>
                <HomeStack />
            </NavigationContainer>
        );
    }
}


const styles = {
    button: {
        // marginTop: -2,
        backgroundColor: '#e0e0e0',
        borderRadius: 100,
        width: 100,
        height: 100,
        alignItems: 'center',
        justifyContent: 'center',
        top: -80,
        position: 'absolute',
        // bottom:40
    }
};
