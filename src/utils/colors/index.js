export const colors = {
    background: '#FFFFFF', //white
    temaUtama: '#fc4903', //red
    merah: '#a82929',
    merahshade: '#cf3c3c',
    abuabutua: '#3d3d3d',
    abuabumuda: '#d3d3d3',
    putih: '#ffffff',
    hitam: '#000000'
}
