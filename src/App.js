import React from 'react';
import { StyleSheet } from 'react-native';
import Router from './router';
import { Provider } from 'react-redux';
import store from './stores';
import codePush from "react-native-code-push";

let codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.IMMEDIATE,
};

const App = () => {
  return (
    <Provider store={store}>
      <Router/>
    </Provider>
  );
};

export default codePush(codePushOptions)(App);
