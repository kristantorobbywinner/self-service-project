import SplashScreen from './SplashScreen';
import HomePage from './HomePage';
import PresencePage from './PresencePage';
import ProfilePage from './ProfilePage';
import AttendanceHistory from './AttendanceHistory';
import DetailPodcastPage from './DetailPodcastPage';
import ListPodcastPage from './ListPodcastPage';
import ListChatPage from './ListChatPage';
import DetailChatPage from './DetailChatPage';
import MyTaskPage from './MyTaskPage';
import LoginPage from './LoginPage';
import ForgetPassword from './ForgetPassword';
import DisplayWeb from './DisplayWeb';
import LeavePage from './LeavePage';
import MedicalBenefitPage from './MedicalBenefitPage';
import MainRoomPage from './MainRoomPage';
import DetailRoomPage from './DetailRoomPage';
import BookingRoomPage from './BookingRoomPage';
import CalendarPage from './CalendarPage';
import MyRoomPage from './MyRoomPage';
import PodcastPage from './PodcastPage';

export {
    MyRoomPage, CalendarPage, BookingRoomPage, DetailRoomPage, MainRoomPage,
    DisplayWeb, MyTaskPage, DetailChatPage, ListChatPage, ListPodcastPage,
    DetailPodcastPage, AttendanceHistory, ProfilePage, SplashScreen, HomePage,
    PresencePage, LoginPage, ForgetPassword, LeavePage, MedicalBenefitPage, PodcastPage
};

