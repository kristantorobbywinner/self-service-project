/* eslint-disable no-alert */
import React, { useState, useCallback, useEffect } from 'react';
import {
    Dimensions,
    StyleSheet,
    Text,
    View,
    ScrollView,
    RefreshControl,
    Alert
} from 'react-native';
import { colors } from '../../utils/colors';
import AsyncStorage from '@react-native-async-storage/async-storage';
import MyBookingsCard from '../../components/MyBookingsCard';
import queryMyBooking from '../../Api/myBookingApi';
import queryDeleteBooking from '../../Api/deleteBookingApi';

const { height } = Dimensions.get('screen');

export default function MyRoomPage() {
    const [refreshing, setRefreshing] = useState(false);
    const [meetingRoom, setMeetingRoom] = useState([]);

    useEffect(() => {
        fetchMyBookings();
    }, []);

    const fetchMyBookings = async () => {
        try {
            // alert('fetch MyBookings proses')
            const token = await AsyncStorage.getItem('@token');
            await queryMyBooking(token)
                .then((response) => {
                    if (response.data == null) {
                        alert('fetch MyBookings failed');
                    } else {
                        { !response.data ? console.warn(response.code) : setMeetingRoom(response.data.data[0].booking); }
                        // alert('fetch MyBookings success')
                    }
                });
        } catch (error) {
            alert(error);
        }
    };

    const onRefresh = useCallback(async () => {
        setRefreshing(true);
        await fetchMyBookings();
        setRefreshing(false);
    }, []);

    const sendDelete = async (id) => {
        try {
            const token = await AsyncStorage.getItem('@token');
            await queryDeleteBooking(token, id)
                .then(async (response) => {
                    if (response.data == null) {
                        alert('delete failed');
                    } else {
                        alert('delete success');
                    }
                });
        } catch (error) {
            alert('delete failed');
        }
    };

    return (
        <View style={{ height: height, backgroundColor: colors.abuabumuda }}>
            <View style={styles.wrapperAtas}>
                <Text style={styles.textAtas}>My Bookings</Text>
            </View>
            <ScrollView
                style={styles.scrollView}
                refreshControl={
                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                }
            >
                {meetingRoom.map((item, index) => {
                    return <MyBookingsCard
                        key={index}
                        id={item.id}
                        name={item.room.name}
                        time={item.time}
                        description={item.description}
                        isProjector={item.is_projector}
                        isHdmi={item.is_hdmi}
                        isBoard={item.is_board}
                        isConsumption={item.is_consumption}
                        consumption={item.consumption}
                        kapasitas={item.room.kapasitas}
                        gedung={item.room.gedung}
                        pressDelete={() => Alert.alert(
                            'warning',
                            'delete booking?',
                            [
                                {
                                    text: 'No',
                                    onPress: () => null
                                },
                                {
                                    text: 'Yes',
                                    // onPress: () => null
                                    onPress: async () => {
                                        await sendDelete(item.id);
                                        await fetchMyBookings();
                                    }
                                }
                            ])}
                    />;
                })}
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    scrollView: {
        paddingHorizontal: 10,
        backgroundColor: colors.abuabumuda,
        paddingBottom: 100,
    },
    userCard: {
        backgroundColor: 'white',
        paddingVertical: 6,
        paddingHorizontal: 6,
        borderRadius: 10,
        marginTop: 10,
        display: 'flex',
        // flexDirection: 'row',
        alignItems: 'center',
        height: 130,
        width: '100%'
    },
    userImage: {
        width: 40,
        height: 40,
        borderRadius: 100
    },
    userCardRight: {
        paddingHorizontal: 10
    },
    wrapperAtas: {
        width: '100%',
        height: 80,
        backgroundColor: colors.merah,
        justifyContent: 'center',
        borderBottomEndRadius: 20,
        borderBottomStartRadius: 20
    },
    textAtas: {
        paddingTop: 10,
        color: colors.putih,
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center'

    }
});
