import React, { useEffect, useState } from "react";
import { Dimensions, StyleSheet, Text, View } from "react-native";
import { FlatList } from "react-native-gesture-handler";
import queryPodcast from "../../Api/podcastApi";
import PodcastCard from "../../components/PodcastCard";
import { colors } from "../../utils";
import AsyncStorage from '@react-native-async-storage/async-storage'
import MyRoomCard from "../../components/MyRoomCard";
import queryGetMeetingRoom from "../../Api/getMeetingRoomApi";
import queryMyBooking from "../../Api/myBookingApi";
import MyBookingsCard from "../../components/MyBookingsCard";
const { width, height } = Dimensions.get('screen');

export default function MyRoomPage() {
  const [meetingRoom, setMeetingRoom] = useState([])
  
  useEffect(() => {
    fetchMyBookings()
  }, [])

  const fetchMyBookings = async () => {
    try {
      const token = await AsyncStorage.getItem('@token')
      await queryMyBooking(token)
        .then((response) => {
          if (response.data == null) {
            alert('fetch MyBookings failed')
          } else {
            { !response.data ? console.warn(response.code) : setMeetingRoom(response.data.data[0]) }
            console.warn(response.data.data[0].booking[0])
            // alert('fetch MyBookings success')
          }
        })
    } catch (error) {
      alert(error)
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.wrapperAtas}>
        <Text style={styles.textAtas}>My Room</Text>
      </View>
      <FlatList
        data={meetingRoom}
        // horizontal
        pagingEnabled
        style={{ flexGrow: 0 }}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{ alignItems: 'center' }}
        snapToInterval={width}
        decelerationRate={0}
        bounces={false}
        renderItem={({ item, index }) => {
          alert(meetingRoom)
          return <MyBookingsCard
            index={index}
            id={item.id}
            name={item.room_id}
            time={item.time}
            description={item.description}
            // kapasitas={item.room.kapasitas}
            // gedung={item.room.gedung}
          />
        }}
        keyExtractor={(item) => item.id}
      // onEndReached={() => handleLoadMore()}
      // onEndReachedThreshold={0.3}
      />
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  description: {
    width: "80%",
    marginTop: 20,
    textAlign: "center"
  },
  player: {
    marginTop: 40
  },
  state: {
    marginTop: 20
  },
  wrapperAtas: {
    width: '100%',
    height: 80,
    backgroundColor: colors.merah,
    justifyContent: 'center',
    borderBottomEndRadius: 20,
    borderBottomStartRadius: 20,
  },
  textAtas: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center'

  }
});
