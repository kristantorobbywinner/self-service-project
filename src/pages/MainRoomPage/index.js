/* eslint-disable no-lone-blocks */
import React, { useEffect, useCallback, useState } from 'react';
import { Dimensions, Platform, StyleSheet, Text, View, RefreshControl, TouchableOpacity } from 'react-native';
import { colors } from '../../utils';
import { Picker } from '@react-native-picker/picker';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import { Button, CheckBoxComp } from '../../components/atoms';
import AsyncStorage from '@react-native-async-storage/async-storage';
import MyRoomCard from '../../components/MyRoomCard';
import { connect } from 'react-redux';
import fetchBookingRoom from '../../stores/actions/bookingRoomAction';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
const { width, height } = Dimensions.get('screen');


const MainRoomPage = (props) => {
    const [refreshing, setRefreshing] = useState(false);
    const [date, setDate] = useState(moment(new Date()).format('YYYY-MM-DD'));
    const [] = useState('');
    const [show, setShow] = useState(false);
    const [building, setBuilding] = useState('A');
    const [capacity, setCapacity] = useState('0');
    const [checked0809, setChecked0809] = React.useState(false);
    const [checked0910, setChecked0910] = React.useState(false);
    const [checked1011, setChecked1011] = React.useState(false);
    const [checked1112, setChecked1112] = React.useState(false);
    const [checked1213, setChecked1213] = React.useState(false);
    const [checked1314, setChecked1314] = React.useState(false);
    const [checked1415, setChecked1415] = React.useState(false);
    const [checked1516, setChecked1516] = React.useState(false);
    const [checked1617, setChecked1617] = React.useState(false);
    const [meetingRoom, setMeetingRoom] = useState([]);
    const [filterRoom, setFilterRoom] = useState([]);
    const filterDateTime = [];

    useEffect(() => {
        // fetchMeetingRoom()
        getData();
    }, []);

    useEffect(() => {
        if (props.bookingRoomStore.payload.data) {
            // setUsers(props.homeProfileStore.payload.data[0]);
            setMeetingRoom(props.bookingRoomStore.payload.data);
            setFilterRoom(props.bookingRoomStore.payload.data);
        }
    }, [props.bookingRoomStore.payload.data]);

    useEffect(() => {
        handleFilterDateTime();
        // handleSearch()
    }, [checked0809, checked0910, checked1011, checked1112, checked1213, checked1314, checked1415, checked1516, checked1617]);

    const onRefresh = useCallback(async () => {
        try {
            setRefreshing(true);
            await getData();
            setRefreshing(false);
        } catch (error) {
            alert(error);
        }
    });

    const getData = async () => {
        try {
            const token = await AsyncStorage.getItem('@token');
            await props.dispatchBookingRoom(token);
        } catch (error) {
            alert(error);
        }
    };

    const showDatePicker = () => {
        setShow(true);
    };

    const hideDatePicker = () => {
        setShow(false);

    };

    const handleConfirm = (item) => {
        // console.log('A date has been picked: ', item);
        setDate(moment(item).format('YYYY-MM-DD'));
        hideDatePicker();
    };

    const handleFilterDateTime = () => {
        { checked0809 ? filterDateTime.push(`${date}T08:00:00.000Z`) : null; }
        { checked0910 ? filterDateTime.push(`${date}T09:00:00.000Z`) : null; }
        { checked1011 ? filterDateTime.push(`${date}T10:00:00.000Z`) : null; }
        { checked1112 ? filterDateTime.push(`${date}T11:00:00.000Z`) : null; }
        { checked1213 ? filterDateTime.push(`${date}T12:00:00.000Z`) : null; }
        { checked1314 ? filterDateTime.push(`${date}T13:00:00.000Z`) : null; }
        { checked1415 ? filterDateTime.push(`${date}T14:00:00.000Z`) : null; }
        { checked1516 ? filterDateTime.push(`${date}T15:00:00.000Z`) : null; }
        { checked1617 ? filterDateTime.push(`${date}T16:00:00.000Z`) : null; }
        // console.warn(filterDateTime)
    };

    const handleSearch = async () => {
        await onRefresh();
        const k = meetingRoom.filter((item) => {
            const timeBook = item.booking.map(function (item) {
                return item.time;
            });
            const res = timeBook.filter(item => filterDateTime.includes(item));
            return item.gedung === building &&
                capacity < item.kapasitas &&
                !res[0];
        }
        );
        setFilterRoom(k);
        setChecked0809(false);
        setChecked0910(false);
        setChecked1011(false);
        setChecked1112(false);
        setChecked1213(false);
        setChecked1314(false);
        setChecked1415(false);
        setChecked1516(false);
        setChecked1617(false);
    };

    return (
        <View style={{ flex: 1 }}>
            <View style={styles.wrapperAtas}>
                <Text style={styles.textAtas}>Booking Meeting Room</Text>
            </View>
            <View style={{ height:height*0.75, flexDirection: 'row', width: width, alignItems: 'center', justifyContent: 'space-evenly' }}>
                <View style={{ alignItems: 'center', width: '50%', height:'100%' }}>
                    <View style={{ alignItems: 'center', alignSelf: 'center', marginTop: 10 }}>
                        <Text style={{}}>Date</Text>
                        <TouchableOpacity
                            style={styles.inputDate}
                            onPress={showDatePicker}
                        >
                            <Text style={{ marginLeft: 1 }} onPress={showDatePicker}>{moment(date).format('YYYY-MM-DD')}</Text>
                            <DateTimePickerModal
                                isVisible={show}
                                mode="date"
                                onConfirm={handleConfirm}
                                onCancel={hideDatePicker}
                            />
                            <Icon style={{ marginLeft: 1 }} name="calendar" onPress={showDatePicker} size={25} color={'#696969'} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ alignItems: 'center', alignSelf: 'center', marginTop: 10 }}>
                        <Text style={{}}>Building</Text>
                        <View style={styles.inputType}>
                            <Picker
                                selectedValue={building}
                                style={{ fontSize: 15, height: '100%', justifyContent: 'space-around' }}
                                onValueChange={(itemValue) =>
                                    setBuilding(itemValue)
                                }
                                itemStyle={{ fontSize: 15 }}
                            >
                                <Picker.Item label="A" value="A" />
                                <Picker.Item label="B" value="B" />
                            </Picker>
                        </View>
                    </View>
                    <View style={{ alignItems: 'center', alignSelf: 'center', marginTop: 10 }}>
                        <Text style={{}}>Capacity</Text>
                        <View style={styles.inputDate}>
                            <TextInput
                                style={{ marginLeft: 8 }}
                                value={capacity}
                                onChangeText={(value) => setCapacity(value)}
                                placeholder={'0'}
                            />
                            <Icon style={{ marginRight: 11 }} name="ios-person" size={25} color={'#696969'} />
                        </View>
                    </View>
                    <View style={{ alignItems: 'center', alignSelf: 'center', marginTop: 10 }}>
                        <Text style={{}}>Time</Text>
                        <View style={styles.inputTime}>
                            <CheckBoxComp
                                onPress={() => { setChecked0809(!checked0809); }}
                                checked={checked0809}
                                text="08:00-09:00"
                            />
                            <CheckBoxComp
                                onPress={() => { setChecked0910(!checked0910); }}
                                checked={checked0910}
                                text="09:00-10:00"
                            />
                            <CheckBoxComp
                                onPress={() => { setChecked1011(!checked1011); }}
                                checked={checked1011}
                                text="10:00-11:00"
                            />
                            <CheckBoxComp
                                onPress={() => { setChecked1112(!checked1112); }}
                                checked={checked1112}
                                text="11:00-12:00"
                            />
                            <CheckBoxComp
                                onPress={() => { setChecked1213(!checked1213); }}
                                checked={checked1213}
                                text="12:00-13:00"
                            />
                            <CheckBoxComp
                                onPress={() => { setChecked1314(!checked1314); }}
                                checked={checked1314}
                                text="13:00-14:00"
                            />
                            <CheckBoxComp
                                onPress={() => { setChecked1415(!checked1415); }}
                                checked={checked1415}
                                text="14:00-15:00"
                            />
                            <CheckBoxComp
                                onPress={() => { setChecked1516(!checked1516); }}
                                checked={checked1516}
                                text="15:00-16:00"
                            />
                            <CheckBoxComp
                                onPress={() => { setChecked1617(!checked1617); }}
                                checked={checked1617}
                                text="16:00-17:00"
                            />
                        </View>
                    </View>
                </View>

                <ScrollView
                    contentContainerStyle={{ marginVertical: 10, }}
                    refreshControl={
                        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                    }
                // horizontal
                >
                    {filterRoom.map(item => {
                        return <MyRoomCard
                            key={item.id}
                            id={item.id}
                            name={item.name}
                            kapasitas={item.kapasitas}
                            gedung={item.gedung}
                            booking={item.booking}
                        />;
                    })}
                </ScrollView>
            </View>

            <View style={styles.wrapperButton}>
                <Button
                    title="search"
                    onPress={() => {
                        handleSearch();
                    }}
                    type="default"
                    white=""
                />
                <View style={{width: 40,}}/>
                <Button
                    title="My Room"
                    onPress={() => {
                        props.navigation.navigate('MyRoomPage');
                    }}
                    type="default"
                    white=""
                />
            </View>




        </View>
    );
};

function mapStateToProps(state) {
    return {
        bookingRoomStore: state.bookingRoomStore
    };
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchBookingRoom: (token) => dispatch(fetchBookingRoom(token))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(MainRoomPage);

const styles = StyleSheet.create({
    wrapperAtas: {
        width: '100%',
        height: 80,
        backgroundColor: colors.merah,
        justifyContent: 'center',
        borderBottomEndRadius: 20,
        borderBottomStartRadius: 20
    },
    textAtas: {
        paddingTop: 10,
        color: colors.putih,
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center'
    },
    inputType: {
        // flex: 0.55,
        overflow: 'hidden',
        paddingHorizontal: 15,
        width: 160,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        justifyContent: 'center'
    },
    inputDate: {
        // flex: 0.55,
        flexDirection: 'row',
        paddingHorizontal: 15,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: 160
    },
    inputTime: {
        // height: 220,
        borderRadius: 25,
        width: 160,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    wrapperButton: {
        flexDirection: 'row',
        backgroundColor: colors.merah,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        justifyContent: 'center',
        height: height*0.1,
        alignItems: 'center'
    }
});
