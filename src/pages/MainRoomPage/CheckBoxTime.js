import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Checkbox } from 'react-native-paper';
const CheckBoxTime = ({onPress,checked0809,text}) => {
    return (
        <View style={styles.inputTime}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Checkbox
                    status={checked0809 ? 'checked' : 'unchecked'}
                    onPress={onPress}
                />
                <Text> {text} </Text>
            </View>

        </View>
    )
}

export default CheckBoxTime

const styles = StyleSheet.create({
    inputTime: {
        flex: 0.55,
        // flexDirection: 'row',
        paddingHorizontal: 15,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
})
