import React, { useState, useCallback, useEffect } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Dimensions, FlatList } from 'react-native';
import HistoryLeaveCard from '../../components/HistoryLeaveCard';
import ApplyLeaveCard from './../../components/ApplyLeaveCard';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import queryHistoryLeave from '../../Api/historyLeaveAPI';
import balanceLeave from '../../Api/balanceLeaveAPI';
import fetchUserProfile from './../../stores/actions/userAction';

const { height } = Dimensions.get('window');
const LeavePage = (props) => {
    const avatarIcon = props.userStore.payload.avatar ? props.userStore.payload.avatar : Image.resolveAssetSource(require('../../assets/images/avatar-user.png')).uri;
    const fullName = props.userStore.payload.name;
    const [isApply, setIsApply] = useState(false);
    const [refreshing, setRefreshing] = useState(false);
    const [historyData, setHistoryData] = useState([]);
    const [balance, setBalance] = useState();
    const [imageHeadersState, setImageHeadersState] = useState({});

    useEffect(() => {
        fetchDataLeave();
        fetchDataBalance();
        setImageHeaders();
    }, []);

    const onRefresh = useCallback(async () => {
        const token = await AsyncStorage.getItem('@token');
        setRefreshing(true);
        fetchDataLeave(token);
        fetchDataBalance(token);
        setRefreshing(false);
    }, []);

    const fetchDataLeave = async () => {
        try {
            const token = await AsyncStorage.getItem('@token');
            await queryHistoryLeave(token)
                .then((response) => {
                    if (response.data == null) {
                        console.log('fetch data failed');
                    } else {
                        !response.data ? console.warn(response.code) : setHistoryData(response.data.data);
                    }
                });
        } catch (error) {
            console.log('fetch history gagal');
        }
    };

    const fetchDataBalance = async () => {
        try {
            const token = await AsyncStorage.getItem('@token');
            await balanceLeave(token)
                .then((response) => {
                    if (response.data == null) {
                        console.log('fetch data failed');
                    } else {
                        !response.data ? console.warn(response.code)
                            : setBalance(() => {
                                for (const item of response.data.data) {
                                    if (item.type === 'Cuti Tahunan Permanen') {
                                        return item.balance;
                                    }
                                }
                            }
                            );
                    }
                });
        } catch (error) {
            console.log('balance history gagal');
        }
    };

    const setImageHeaders = async () => {
        try {
            const token = await AsyncStorage.getItem('@token');

            const headers = {
                'api_key': '76f8a1fab09bc13f2e48be45689dd074',
                'Authorization': `Bearer ${token}`
            };

            setImageHeadersState(headers);
        } catch (error) {
            throw error;
        }
    };

    return (
        <View>
            <View style={{ height: 0.25 * height, alignItems: 'center', justifyContent: 'flex-end' }}>
                <Image
                    source={{
                        uri: avatarIcon,
                        headers: imageHeadersState
                    }}
                    width={1}
                    height={1}
                    style={styles.avatar}
                />
                <Text style={{ textAlign: 'center', marginTop: 10, marginBottom: 5, fontSize: 20, fontWeight: 'bold' }}>{fullName}</Text>
            </View>
            <View style={{ height: 0.1 * height, alignItems: 'center' }}>
                <View style={{ flex: 0.5, flexDirection: 'row' }}>
                    <Text style={{ textAlign: 'center', fontSize: 15 }}>Remaining Leave Balance : </Text>
                    <Text style={{ textAlign: 'center', fontSize: 15 }}>{balance} Days</Text>
                </View>
                <View style={{ flex: 0.5, justifyContent: 'flex-end' }}>
                    <Text style={{ textAlign: 'center', fontSize: 24, fontWeight: 'bold', marginBottom: 10 }}>2020</Text>
                </View>
            </View>
            <View style={{ height: 0.65 * height, backgroundColor: '#A82929', borderTopLeftRadius: 50, borderTopRightRadius: 50 }}>
                <View style={{ flex: 0.1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ textAlign: 'center', fontSize: 20, color: '#FFFFFF' }}>History Leave</Text>
                </View>
                <View style={{ flex: 0.9 }}>
                    <FlatList
                        data={historyData}
                        renderItem={({ item, index }) => {
                            return <HistoryLeaveCard
                                index={index}
                                data={item}
                                typeLeave={item.type_approval}
                                startDateLeave={item.start_date}
                                endDateLeave={item.end_date}
                                durationLeave={item.duration}
                            />;
                        }}
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                        keyExtractor={(item) => item.id.toString()}
                    />
                </View>
            </View>
            <TouchableOpacity
                style={styles.button}
                onPress={() =>
                    setIsApply(true)
                    }>
                <Text style={{ textAlign: 'center', color: '#FFFFFF', fontSize: 15 }}>Apply For Leave</Text>
            </TouchableOpacity>

            {isApply ?
                <ApplyLeaveCard
                    backPress={async () => {
                        try {
                            setIsApply(false);
                            fetchDataLeave();
                            fetchDataBalance();
                        } catch (error) {
                            throw error;
                        }
                    }}
                />
                : null}
        </View>
    );
};

function mapStateToProps(state) {
    return {
        userStore: state.userStore
    };
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchUserProfile: (token) => dispatch(fetchUserProfile(token))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(LeavePage);

const styles = StyleSheet.create({
    avatar: {
        height: 100,
        width: 100,
        borderRadius: 50
    },
    button: {
        justifyContent: 'center',
        alignSelf: 'center',
        position: 'absolute',
        backgroundColor: '#000000',
        borderRadius: 25,
        height: 50,
        width: 250,
        bottom: 40
    }
});
