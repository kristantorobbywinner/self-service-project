import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { Dimensions, Alert, StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { ExpandableCalendar, AgendaList, CalendarProvider, WeekCalendar } from 'react-native-calendars';
import { colors } from '../../utils';
import moment from 'moment';
const lightThemeColor = '#EBF9F9';
import Icon from 'react-native-vector-icons/Ionicons';
import BookingRoomCard from '../../components/BookingRoomCard';
import { connect } from 'react-redux';
import fetchBookingRoom from '../../stores/actions/bookingRoomAction';
const { width, height } = Dimensions.get('screen');

const DetailRoomPage = (props) => {
    const testIDs = require('./testIDs');
    const [isBook, setIsBook] = useState(false);
    const [] = useState([]);
    const [namaMeetingRoom, setNamaMeetingRoom] = useState('');
    const [result, setResult] = useState([]);

    useEffect(() => {
        if (props.bookingRoomStore.payload.data) {
            fetchMeetingRoom();
        }
    }, [props.bookingRoomStore.payload.data]);


    const fetchMeetingRoom = async () => {
        try {
            const filt = await props.bookingRoomStore.payload.data.filter(item => item.id == props.route.params.id);
            setNamaMeetingRoom(filt[0].name);

            const dataDate = filt[0].booking.map((e) => {
                return {
                    title: moment(e.time).utc().format('YYYY-MM-DD'),
                    data: [{ hour: moment(e.time).utc().format('HH:mm'), duration: '1h', title: 'Booked' }]
                };
            });

            const resultFilter = dataDate.reduce((acc, d) => {
                const found = acc.find(a => a.title === d.title);
                const data = d.data;
                if (!found) {
                    acc.push({ title: d.title, data: data });
                }
                else {
                    found.data.push(...data);
                }
                return acc;
            }, []);

            setResult(resultFilter);
        } catch (error) {
            alert(error);
        }
    };

    // eslint-disable-next-line no-unused-vars
    function getFutureDates(days) {
        const array = [];
        for (let index = 1; index <= days; index++) {
            const date = new Date(Date.now() + 864e5 * index); // 864e5 == 86400000 == 24*60*60*1000
            const dateString = date.toISOString().split('T')[0];
            array.push(dateString);
        }
        return array;
    }

    // eslint-disable-next-line no-unused-vars
    function getPastDate(days) {
        return new Date(Date.now() - 864e5 * days).toISOString().split('T')[0];
    }

    const onDateChanged = () => {
    };

    const onMonthChange = () => {
    };


    const itemPressed = (id) => {
        Alert.alert(id);
    };

    const renderEmptyItem = () => {
        return (
            <View style={styles.emptyItem}>
                <Text style={styles.emptyItemText}>No Events Planned</Text>
            </View>
        );
    };

    const renderItem = ({ item }) => {
        if (_.isEmpty(item)) {
            return renderEmptyItem();
        }
        return (
            <TouchableOpacity onPress={() => itemPressed(item.title)} style={styles.item} testID={testIDs.agenda.ITEM}>
                <View>
                    <Text style={styles.itemHourText}>{item.hour}</Text>
                    <Text style={styles.itemDurationText}>{item.duration}</Text>
                </View>
                <Text style={styles.itemTitleText}>{item.title}</Text>
            </TouchableOpacity>
        );
    };

    const getMarkedDates = () => {
        const marked = {};
        result.forEach(item => {
            if (item.data && item.data.length > 0 && !_.isEmpty(item.data[0])) {
                marked[item.title] = { marked: true };
            } else {
                marked[item.title] = { disabled: true };
            }
        });
        return marked;
    };

    return (
        <View style={{ width: width, height: height * 0.95 }}>
            <CalendarProvider
                date={result[0] ? result[0].title : new Date().toISOString().split('T')[0]}
                onDateChanged={() => onDateChanged()}
                onMonthChange={() => onMonthChange()}
                // showTodayButton
                disabledOpacity={0.6}
            >
                <View style={styles.wrapperAtas}>
                    <Text style={styles.textAtas}>{namaMeetingRoom}</Text>
                </View>
                {props.weekView ? (
                    <WeekCalendar testID={testIDs.weekCalendar.CONTAINER} firstDay={1} markedDates={getMarkedDates()} />
                ) : (
                        <ExpandableCalendar
                            testID={testIDs.expandableCalendar.CONTAINER}
                            disableAllTouchEventsForDisabledDays
                            firstDay={1}
                            markedDates={getMarkedDates()}
                        />
                    )}
                <AgendaList
                    sections={result}
                    // extraData={}
                    renderItem={(item) => renderItem(item)}
                />

                <TouchableOpacity
                    style={{ position: 'absolute', bottom: 20, right: 20, flexDirection: 'row', alignItems: 'center' }}
                    onPress={() => {
                        setIsBook(true);
                        // props.navigation.navigate('BookingRoomPage', {
                        //     id: props.route.params.id,
                        //     name: props.route.params.name,
                        //     kapasitas: props.route.params.kapasitas,
                        //     gedung: props.route.params.gedung,
                        //     booking: props.route.params.booking
                        // })
                    }
                    }
                >
                    <Text style={{ margin: 10, fontSize: 20, fontWeight: 'bold' }}>Book</Text>
                    <Icon name={'ios-add-circle'} size={60} color={colors.merah} />
                </TouchableOpacity>



            </CalendarProvider>
            {isBook && (
                <BookingRoomCard
                    id={props.route.params.id}
                    backPress={() => {
                        setIsBook(false);
                    }}
                />
            )}
        </View>

    );
};

function mapStateToProps(state) {
    return {
        bookingRoomStore: state.bookingRoomStore
    };
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchBookingRoom: (token) => dispatch(fetchBookingRoom(token))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailRoomPage);

const styles = StyleSheet.create({
    calendar: {
        paddingLeft: 20,
        paddingRight: 20
    },
    section: {
        backgroundColor: lightThemeColor,
        color: 'grey',
        textTransform: 'capitalize'
    },
    item: {
        padding: 20,
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        flexDirection: 'row'
    },
    itemHourText: {
        color: 'black'
    },
    itemDurationText: {
        color: 'grey',
        fontSize: 12,
        marginTop: 4,
        marginLeft: 4
    },
    itemTitleText: {
        color: 'black',
        marginLeft: 16,
        fontWeight: 'bold',
        fontSize: 16
    },
    itemButtonContainer: {
        flex: 1,
        alignItems: 'flex-end'
    },
    emptyItem: {
        paddingLeft: 20,
        height: 52,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey'
    },
    emptyItemText: {
        color: 'lightgrey',
        fontSize: 14
    },
    wrapperAtas: {
        width: '100%',
        height: 80,
        backgroundColor: colors.merah,
        justifyContent: 'center',
        borderBottomEndRadius: 20,
        borderBottomStartRadius: 20
    },
    textAtas: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        paddingTop: 30,
        color:'#FFFFFF'

    }
});
