import { combineReducers } from 'redux';
import bookingRoomStoreReducer from './bookingRoomStoreReducer';
import homeProfileStoreReducer from './homeProfileStoreReducer';
import userStoreReducer from './userStoreReducer'

const reducers = {
  homeProfileStore: homeProfileStoreReducer,
  userStore: userStoreReducer,
  bookingRoomStore: bookingRoomStoreReducer,
};

const rootReducer = combineReducers(reducers);

export default rootReducer;
