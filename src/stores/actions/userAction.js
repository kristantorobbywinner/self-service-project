import getProfileInfo from '../../Api/getProfileInfoApi';

export default function fetchUserProfile(token) {
  return async (dispatch) => {
    dispatch({
      type: 'FETCH_USER_REQUEST'
    });

    try {
      const result = await getProfileInfo(token);

      if (result.status === 200) {
        dispatch({
          type: 'FETCH_USER_SUCCESS',
          payload: result.data.data[0]
        });
      } else {
        dispatch({
          type: 'FETCH_USER_FAILED',
          error: result.data
        });
      }
    } catch (err) {
        dispatch({
        type: 'FETCH_USER_FAILED',
        error: err
      });
    }
  };
}

