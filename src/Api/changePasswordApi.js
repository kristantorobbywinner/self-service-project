import axios from 'axios';

export default async function changePassword(token, oldPassword, newPassword, confirmPassword) {
    try {
        const baseUrl = 'http://139.59.124.53:4444/api/auth/edit';
        const headers = {
            'api_key': '76f8a1fab09bc13f2e48be45689dd074',
            'Authorization': `Bearer ${token}`
        };

        const data = {
            'oldpassword': oldPassword,
            'password': newPassword,
            'confirmpassword': confirmPassword
        };

        const result = await axios({
            method: 'POST',
            url: baseUrl,
            headers,
            data
        });

        return result;

    } catch (error) {
        throw error;
    }
}
