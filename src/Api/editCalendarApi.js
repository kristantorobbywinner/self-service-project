import axios from 'axios';

export default async function editNote(token, id, date, description) {
    try {
        const baseUrl= 'http://139.59.124.53:4444/api/calendar/notes/edit'
        const headers = {
            'api_key': `76f8a1fab09bc13f2e48be45689dd074`,
            'Authorization': `Bearer ${token}`,
        }

        const data = {
            id: id,
        	date: date.toString(),
            description: description,
        }

        const result = await axios({
            method: 'POST',
            url: baseUrl,
            headers,
            data,
        });

        return result

    } catch (error) {
        throw error;
    }
}