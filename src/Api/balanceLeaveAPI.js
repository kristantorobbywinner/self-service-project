import axios from 'axios';

export default async function balanceLeave(token) {
    try {
        const baseUrl = 'http://139.59.124.53:4444/api/user_leave/getBalance';
        const headers = {
            'api_key': '76f8a1fab09bc13f2e48be45689dd074',
            'Authorization': `Bearer ${token}`
        };
        // console.log('balance');
        const result = await axios({
            method: 'GET',
            url: baseUrl,
            headers
        });
        // console.log(result);
        return (result);

    } catch (error) {
        throw error;
    }
}
