import axios from 'axios';

export default async function saveReminder(token, dateStart, dateEnd, timeStart, timeEnd, description) {
    try {
        const baseUrl= 'http://139.59.124.53:4444/api/calendar/reminder/insert'
        const headers = {
            'api_key': `76f8a1fab09bc13f2e48be45689dd074`,
            'Authorization': `Bearer ${token}`,
        }

        const data = {
            time_start: dateStart + ' ' + timeStart,
            time_stop: dateEnd + ' ' + timeEnd,
            description: description,
        }


        const result = await axios({
            method: 'POST',
            url: baseUrl,
            headers,
            data,
        });

        return result

    } catch (error) {
        throw error;
    }
}
