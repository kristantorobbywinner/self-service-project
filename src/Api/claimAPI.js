import axios from 'axios';

export default async function claim(type, date, usage, token) {
    try {
        const baseUrl = 'http://139.59.124.53:4444/api/insurance/insertclaim';
        const headers = {
            'API_KEY': '76f8a1fab09bc13f2e48be45689dd074',
            'Authorization': `Bearer ${token}`
        };

        console.log(`isi data ${type}, ${date}, ${usage}, ${token}`);

        const data = {
            type: type,
            date: '' + date,
            usage: usage
        };
        console.log(data);

        const result = await axios({
            method: 'POST',
            url: baseUrl,
            headers,
            data
        });

        console.log(`result ${result}`);

        return result;
    } catch (error) {
        throw error;
    }
}
