import axios from 'axios'

export default async function queryDeleteBooking(token,id) {
    try {
        const baseUrl = 'http://139.59.124.53:4444/api/meeting/delete'
        const headers = {
            'api_key': `76f8a1fab09bc13f2e48be45689dd074`,
            'Authorization': `Bearer ${token}`
        }

        const data =
        {
            "id":id //2
        }

        const result = await axios({
            method: 'POST',
            url: baseUrl,
            headers,
            data,
        })

        return (result)

    } catch (error) {
        throw error
    }
}