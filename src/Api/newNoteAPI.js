
export default async function saveNote(token, date, description) {
    try {
        const baseUrl = 'http://139.59.124.53:4444/api/calendar/notes';
        const headers = {
            'api_key': '76f8a1fab09bc13f2e48be45689dd074',
            'Authorization': `Bearer ${token}`,
        };

        const data = {
        	date, description,
        };

        const result = await axios({
            method: 'POST',
            url: baseUrl,
            headers,
            data,
        });

        return result;

    } catch (error) {
        throw error;
    }
}
