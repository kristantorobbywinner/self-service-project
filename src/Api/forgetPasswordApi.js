import axios from 'axios'

export default async function doResetPassword(email) {
    try{
        const baseUrl = 'http://139.59.124.53:4444/api/auth/forget'

        const headers = {
            'api_key': `76f8a1fab09bc13f2e48be45689dd074`,
        }

        const data = {
            'email': email,
        }

        const result = await axios({
            method: 'POST',
            url: baseUrl,
            headers,
            data
        })
        //console.log(result.data.code)
        return result

    } catch(error){
        throw error
    }
}