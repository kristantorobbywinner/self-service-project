import axios from 'axios'

export default async function uploadAvatar(token, imageBase64){
    try {
        const baseUrl = 'http://139.59.124.53:4444/api/upload'
        const headers = {
            'API_KEY': `76f8a1fab09bc13f2e48be45689dd074`,
            'Authorization': `Bearer ${token}`,
        }
        const data = {
            'avatar': 'data:image/jpeg;base64,'+''+imageBase64,
        }
        const result = await axios({
            method: 'POST',
            url: baseUrl,
            headers,
            data,
        })

        return result

    } catch (error) {
        throw error
    }
}