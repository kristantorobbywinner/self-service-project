import React, { useState } from 'react';
import { StyleSheet, View,TextInput } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { colors } from '../../../utils';
import Icon from 'react-native-vector-icons/Ionicons';

const Input = ({ type, placeholder, notShowPassword, onPress, searchPress,...rest }) => {

    const [focus,setFocus] = useState(false);
    const [focusPassword,setFocusPassword] = useState(false);

    if (type === 'search') {
        return (
            <View style={styles.wrapper(focus)}>
                <TouchableOpacity onPress={searchPress}>
                {type === 'search' ? <Icon name="ios-search" size={20} color={'#2E3E5C'} /> : null}
                </TouchableOpacity>
                <TextInput
                    style={styles.textInput}
                    placeholder={placeholder}
                    // placeholderTextColor={colors.outline}
                    onFocus={()=>setFocus(true)}
                    onBlur={()=>setFocus(false)}
                    {...rest}
                />
            </View>
        );
    }

    if (type === 'email' || type === 'name' || type === 'age' || type === 'amount'){
        return (
            <View style={styles.wrapper(focus)}>
                {type === 'email' ? <Icon name="ios-mail" size={20} color={'#2E3E5C'} /> : null}
                {type === 'name' ? <Icon name="ios-person" size={20} color={'#2E3E5C'} /> : null}
                {type === 'age' ? <Icon name="ios-hand-left" size={20} color={'#2E3E5C'} /> : null}
                {type === 'search' ? <Icon name="ios-search" size={20} color={'#2E3E5C'} /> : null}
                {type === 'amount' ? <Icon name="ios-search" size={20} color={'#2E3E5C'} /> : null}
                <TextInput
                    style={styles.textInput}
                    autoCapitalize="none"
                    placeholder={placeholder}
                    // placeholderTextColor={colors.outline}
                    onFocus={()=>setFocus(true)}
                    onBlur={()=>setFocus(false)}
                    {...rest}
                />
            </View>
        );
    }

    if (type === 'password') {
        return (
            <View style={styles.wrapper(focusPassword)}>
                <Icon name="ios-key" size={20} color="#2E3E5C" />
                <TextInput
                    style={styles.textInput}
                    placeholder={placeholder}
                    // placeholderTextColor={colors.outline}
                    secureTextEntry={notShowPassword}
                    onFocus={()=>setFocusPassword(true)}
                    onBlur={()=>setFocusPassword(false)}
                    {...rest}
                />
                <TouchableOpacity style={{ paddingRight: 20 }} onPress={onPress}>
                    <Icon name="ios-eye" size={20} color={notShowPassword ? '#2E3E5C' : 'green'} />
                </TouchableOpacity>
            </View>
        );
    }

    if (type === 'description') {
        return (
            <View style={styles.wrapper(focusPassword)}>
                <Icon name="ios-key" size={20} color="#2E3E5C" />
                <TextInput
                    style={styles.textInput}
                    placeholder={placeholder}
                    // placeholderTextColor={colors.outline}
                    secureTextEntry={notShowPassword}
                    onFocus={()=>setFocusPassword(true)}
                    onBlur={()=>setFocusPassword(false)}
                    {...rest}
                />
                <TouchableOpacity style={{ paddingRight: 20 }} onPress={onPress}>
                    <Icon name="ios-eye" size={20} color={notShowPassword ? '#2E3E5C' : 'green'} />
                </TouchableOpacity>
            </View>
        );
    }

};

export default Input;


const styles = StyleSheet.create({
    wrapper: (focus)=>{
        return {
        // flex:1,
        flexDirection: 'row',
        height: 40,
        borderColor: focus ? colors.temaModal : colors.temaCard,
        borderRadius: 10,
        paddingLeft: 12.75,
        alignItems: 'center',
        backgroundColor: colors.temaCard
    };},
    textInput: {
        flex: 1,
        fontSize: 16,
        marginLeft: 18.75,
        alignItems: 'center'


    }
});
