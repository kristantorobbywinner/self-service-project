import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import { colors } from '../../../utils';

const Option = ({ options, onChange }) => {
    const [activeOption, setActiveOption] = useState('Pop News');

    return (
        <View style={{ justifyContent: 'center', flexDirection: 'row' }}>

                {options.map((option) => (
                    <TouchableOpacity
                        style={styles.wrapperTombol}
                        key={option}
                        onPress={() => {
                            onChange(option);
                            setActiveOption(option);
                        }}
                    >
                        <View style={styles.tombol(activeOption, option)}>
                            <Icon name="ios-tv" size={15} color={activeOption === option ? 'white' : 'black'} />
                            <Text
                                style={styles.text(activeOption, option)}
                            >
                                {option}
                            </Text>
                        </View>
                    </TouchableOpacity>
                ))}

        </View>
    );
};

export default Option;

const styles = StyleSheet.create({
    tombol: (activeOption, option) => {
        return {
            flexDirection: 'row',
            backgroundColor: activeOption === option ? colors.hitam : colors.putih,
            width: 170,
            height: 32,
            borderRadius: 6,
            marginRight: 6,
            alignItems: 'center',
            justifyContent: 'space-around',
            borderWidth: 1

        };
    },
    text: (activeOption, option) => {
        return {
            textAlign: 'center',
            fontWeight: activeOption === option ? 'bold' : '200',
            fontSize: 13,
            color: activeOption === option ? 'white' : 'black'

        };
    },
    wrapperTombol: {

    }
});
