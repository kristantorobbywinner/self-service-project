import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity } from 'react-native';
import { useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons'
import moment from 'moment';
import { colors } from '../utils';
const { width, height } = Dimensions.get('screen');

const MyRoomCard = (props) => {
    const navigation = useNavigation();

    return (
        <TouchableOpacity
            onPress={() => navigation.navigate("DetailRoomPage", {
                id:props.id, 
                name:props.name,
                kapasitas:props.kapasitas,
                gedung:props.gedung,
                booking:props.booking
             })}
        >
            <View style={styles.wrapper}>
                {/* <Image
                    source={{ uri: props.artwork }}
                    style={styles.imageStyle} /> */}
                <View style={styles.wrapperText}>
                    <Text style={{
                        fontSize: 14,
                        width: '90%',
                        color: colors.merah,
                        fontWeight: 'bold'
                    }}
                        ellipsizeMode="tail"
                        numberOfLines={3}
                    >{props.name}</Text>
                    <Text style={{ fontSize: 12, color: colors.hitam }}>{`gedung: ${props.gedung}`}</Text> 
                    <Text style={{ fontSize: 12, color: colors.hitam }}>{`capacity: ${props.kapasitas}`}</Text>
                </View>
            </View>

        </TouchableOpacity>
    )
}

export default MyRoomCard

const styles = StyleSheet.create({
    wrapper: {
        marginTop:10,
        marginHorizontal: width*0.04,
        marginBottom: 0,
        alignContent: 'center',
        borderRadius: 10,
        backgroundColor: colors.abuabumuda,
        elevation: 8,
        paddingVertical: 20,
        paddingLeft: 5,
    },
    imageStyle: {
        width: "95%",
        height: 100,
        margin: 5,
        borderRadius: 10
    },
    wrapperText:{ 
        justifyContent: 'space-between', 
        bottom:5,
        left:5,
        width:width*0.5-15
    }

})