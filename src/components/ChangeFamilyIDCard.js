import AsyncStorage from '@react-native-async-storage/async-storage'
import React, {useState} from 'react'
import {View, Text, TextInput, TouchableOpacity, StyleSheet} from 'react-native'
import editProfile from '../Api/editProfileAPI'
import {colors} from '../utils/'

const ChangeFamilyIDCard = ({address, backPress}) => {
    const [familyId, setFamilyId] = useState('')

    const changeFamilyIDPressed = async () => {
        try {
            const token = await AsyncStorage.getItem('@token')

            await editProfile(token,familyId, address)
                .then((response) => {
                    if(response.status == 200) {
                        backPress()
                    } else {
                        alert('Change Family Identifier Failed')
                    }
                })
        } catch (error) {
            throw error
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.wrapper}>
                <View style={styles.formContainer}>
                    <TextInput
                        placeholder={'Family ID / Nomor KK'}
                        value={familyId}
                        autoCorrect={false}
                        onChangeText={(value)=> setFamilyId(value)}
                        style={styles.form}/>
                </View>

                <TouchableOpacity
                    style={styles.submitButton}
                    onPress={changeFamilyIDPressed}>
                    <Text style={{color: colors.putih}}>Change Family Identifier</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.submitButton}
                    onPress={backPress}>
                    <Text style={{color: colors.putih}}>Cancel</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default ChangeFamilyIDCard

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        backgroundColor: '#000000bc',
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
    },
    wrapper:{
        borderRadius: 20,
        padding: 20,
        width: '80%',
        marginVertical: 40,
        alignItems: 'center', 
        backgroundColor: colors.abuabumuda,
    },
    formContainer:{
        marginTop: 20,
        width: '100%',
        padding: 2,
        borderRadius: 20,
        backgroundColor: colors.putih,
    },
    form:{
        padding: 10,
        textAlignVertical: 'top',
        width: '100%',
        height: 80,
    },
    submitButton:{
        padding: 10,
        width: '80%',
        marginTop: 20,
        borderRadius: 40,
        alignItems: 'center',
        backgroundColor: colors.abuabutua,
    }
})