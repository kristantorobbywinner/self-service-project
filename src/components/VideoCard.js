import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity } from 'react-native';
import { useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons'
import moment from 'moment';
import { colors } from '../utils';
const { width, height } = Dimensions.get('screen');

const VideoCard = (props) => {
    const navigation = useNavigation();

    return (
        <TouchableOpacity
            onPress={() => navigation.navigate("DisplayWeb", { url: props.url, title: props.title })}
        >
            <View style={styles.wrapper}>
                <Image
                    source={{ uri: props.imageMC }}
                    style={styles.imageStyle} />
                {/* <View style={{ flexDirection: 'row' }}> */}

                <View style={{ justifyContent: 'space-between' }}>
                    <Text style={{
                        fontSize: 20,
                        width: '98%',
                        color: 'black',
                        fontWeight: 'bold'
                    }}
                        ellipsizeMode="tail"
                        numberOfLines={3}
                    >{props.title}</Text>
                    <Text style={{ fontSize: 12, color: 'black' }}>{props.source}</Text>
                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Text>{props.index}</Text>
                        <Icon name='md-calendar' size={15} color={colors.default} />
                        <Text style={{ fontSize: 12, color: 'black', marginLeft: 5 }}>{moment(props.publishedAt, "YYYYMMDD").fromNow()}</Text>

                    </View>
                    {/* </View> */}
                </View>

                <Text
                    style={{ fontSize: 13, color: 'black', padding: 5, fontWeight: 'bold' }}
                    ellipsizeMode="tail"
                    numberOfLines={3}
                >
                    {props.description}
                </Text>


            </View>

        </TouchableOpacity>
    )
}

export default VideoCard

const styles = StyleSheet.create({
    wrapper: {
        margin: width*0.1,
        marginBottom: 0,
        alignContent: 'center',
        borderColor: colors.default,
        borderWidth: 3,
        borderRadius: 10,
        backgroundColor: 'white',
        elevation: 8,
        padding: 4,
        width:width*0.8,
        height:height*0.6


    },
    imageStyle: {
        width: "98%",
        height: 200,
        margin: 5,
        borderRadius: 10
    }

})