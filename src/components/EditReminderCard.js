/* eslint-disable no-alert */
import React, { useState } from 'react';
import { Dimensions, StyleSheet, Text, TouchableOpacity, View, TouchableWithoutFeedback, Platform } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { colors } from '../utils/colors';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';
import { Button } from '../components/atoms/';
import AsyncStorage from '@react-native-async-storage/async-storage';
import addReminder from '../Api/addReminderApi';
import editReminder from '../Api/editReminderApi';

const { width, height } = Dimensions.get('screen');

const EditReminderCard = (props) => {
    const [description, setDescription] = useState(props.data.description);
    const [startDate, setStartDate] = useState(moment(props.data.time_start).utc().format('YYYY-MM-DD HH:mm'));
    const [startTime, setStartTime] = useState(moment(props.data.time_start).utc().format('YYYY-MM-DD HH:mm'));
    const [endDate, setEndDate] = useState(moment(props.data.time_stop).utc().format('YYYY-MM-DD HH:mm'));
    const [endTime, setEndTime] = useState(moment(props.data.time_stop).utc().format('YYYY-MM-DD HH:mm'));
    const [mode, setMode] = useState('');
    const [show, setShow] = useState(false);
    const [showTime, setShowTime] = useState(false);
    const [isStart, setIsStart] = useState(true);

    const pickDate = (event, selectedDate) => {
        const currentDate = selectedDate || startDate;
        setShow(Platform.OS === 'ios');

        if (isStart) {
            setStartDate(moment(currentDate).utc(-7).format('YYYY-MM-DD HH:mm'));
        }
        else {
            setEndDate(moment(currentDate).utc(-7).format('YYYY-MM-DD HH:mm'));
        }
    };

    const pickTime = (event, selectedDate) => {
        const currentDate = selectedDate || startDate;
        setShowTime(Platform.OS === 'ios');

        if (isStart) {
            setStartTime(moment(currentDate).utc(-7).format('YYYY-MM-DD HH:mm'));
        }
        else {
            setEndTime(moment(currentDate).utc(-7).format('YYYY-MM-DD HH:mm'));
        }
    };


    const sendEditReminder = async (token, time_start, time_stop, description,id) => {
        try {
            await editReminder(token, time_start, time_stop, description,id)
                .then(async (response) => {
                    // console.warn(response);
                    if (response.data == null) {
                        alert('submit failed');
                    } else {
                        alert('submit berhasil');
                    }
                });
        } catch (error) {
            alert(error);
        }
    };

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
        // console.log(date);
    };
    const showTimeMode = (currentMode) => {
        setShowTime(true);
        setMode(currentMode);
        // console.log(date);
    };

    const showStartDatepicker = () => {
        showMode('date');
        setIsStart(true);
    };

    const showStartTimepicker = () => {
        showTimeMode('time');
        setIsStart(true);
    };

    const showEndDatepicker = () => {
        showMode('date');
        setIsStart(false);
    };

    const showEndTimepicker = () => {
        showTimeMode('time');
        setIsStart(false);
    };

    return (
        <TouchableOpacity
            style={styles.container}
            onPress={props.backPress}
        >
            <TouchableOpacity
                style={styles.buttonBack}
                onPress={props.backPress}>
                <Icon name="arrow-back-outline" size={40} color={'#FFFFFF'} />
            </TouchableOpacity>
            <TouchableWithoutFeedback onPress={() => null}>
                <View style={styles.wrapperBooking}>
                    <Text style={styles.text}>Description</Text>
                    <View style={styles.input2}>
                        <TextInput
                            // style={styles.input2}
                            onChangeText={(value) => setDescription(value)}
                            value={description}
                            placeholder={'Write your needs'}
                            placeholderTextColor="#979797"
                            textAlignVertical="top"
                            multiline={true}
                            onSubmitEditing={() => {
                                null;
                            }}
                        />
                    </View>
                    <Text style={styles.text}>Start :</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'flex-end' }}>
                        <View style={styles.inputDate}>
                            <Text style={{ marginLeft: 8 }} onPress={showStartDatepicker}>{moment(startDate).format('YYYY-MM-DD')}</Text>
                            <Icon style={{ marginRight: 5 }} name="calendar" onPress={showStartDatepicker} size={25} color={'#696969'} />
                        </View>
                        {/* <Text style={styles.text}>Time</Text> */}
                        <View style={styles.inputDate}>
                            <Text style={{ marginLeft: 8 }} onPress={showStartTimepicker}>{moment(startTime).format('HH:mm')}</Text>
                            <Icon style={{ marginRight: 5 }} name="time" onPress={showStartTimepicker} size={25} color={'#696969'} />
                        </View>
                    </View>
                    <Text style={styles.text}>End :</Text>
                    <View style={{ flexDirection: 'row', alignContent: 'center', alignItems: 'center' }}>
                        <View style={styles.inputDate}>
                            <Text style={{ marginLeft: 8 }} onPress={showEndDatepicker}>{moment(endDate).format('YYYY-MM-DD')}</Text>
                            <Icon style={{ marginRight: 5 }} name="calendar" onPress={showEndDatepicker} size={25} color={'#696969'} />
                        </View>
                        {/* <Text style={styles.text}>Time</Text> */}
                        <View style={styles.inputDate}>
                            <Text style={{ marginLeft: 8 }} onPress={showEndTimepicker}>{moment(endTime).format('HH:mm')}</Text>
                            <Icon style={{ marginRight: 5 }} name="time" onPress={showEndTimepicker} size={25} color={'#696969'} />
                        </View>
                    </View>
                    <Button
                        title={'Edit Reminder'}
                        onPress={async () => {
                            const token = await AsyncStorage.getItem('@token');
                            const time_start = `${moment(startDate).format('YYYY-MM-DD')} ${moment(startTime).format('HH:mm')}`;
                            const time_stop = `${moment(endDate).format('YYYY-MM-DD')} ${moment(endTime).format('HH:mm')}`;
                            await sendEditReminder(token, time_start, time_stop, description,props.data.id);
                            props.backPress;
                        }}
                        type={'default'}
                        white={false}
                    />
                </View>
            </TouchableWithoutFeedback>

            {show && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={new Date()}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={pickDate}
                />
            )}
            {showTime && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={new Date()}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={pickTime}
                />
            )}
        </TouchableOpacity>
    );
};

export default EditReminderCard;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        backgroundColor: '#AAAAAAdd',
        width: width,
        height: height,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 10
    },
    buttonBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: 50,
        height: 50,
        backgroundColor: '#000000',
        borderRadius: 25,
        left: 20,
        top: 20
    },
    wrapperAtas: {
        width: '100%',
        height: 80,
        backgroundColor: colors.merah,
        justifyContent: 'center',
        borderBottomEndRadius: 20,
        borderBottomStartRadius: 20
    },
    text: {
        fontWeight: 'bold',
        fontSize: 17,
        textAlign: 'center'
    },
    input2: {
        paddingHorizontal: 10,
        margin: 14,
        width: width * 0.9,
        height: 60,
        borderRadius: 10,
        backgroundColor: '#dddddd'
    },
    inputDate: {
        marginVertical: 14,
        width: width * 0.4,
        paddingHorizontal: 10,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    inputTime: {
        margin: 14,
        width: width * 0.4,
        paddingHorizontal: 15,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        justifyContent: 'center'
    },
    inputFasilitas: {
        width: width * 0.5,
        marginVertical: 14,
        marginRight: 10,
        padding: 5,
        height: 180,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        alignItems: 'flex-start'
    },
    wrapperBooking: {
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'space-between',
        margin: 10,
        borderWidth: 4,
        borderColor: colors.merah,
        height: height * 0.55,
        backgroundColor: 'white',
        borderRadius: 30,
        paddingVertical: 20
    }
});
