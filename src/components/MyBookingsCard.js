import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'
import moment from 'moment';
import { colors } from '../utils';
import AsyncStorage from '@react-native-async-storage/async-storage';
import queryDeleteBooking from '../Api/deleteBookingApi';
import * as Animatable from 'react-native-animatable';
const { width, height } = Dimensions.get('screen');

const MyBookingsCard = (props) => {

    const sendDelete = async (id) => {
        try {
            const token = await AsyncStorage.getItem('@token')
            await queryDeleteBooking(token,id)
                .then(async (response) => {
                    if (response.data == null) {
                        alert('delete failed')
                    } else {
                        alert('delete success')
                    }
                })
        } catch (error) {
            alert('delete failed')
        }
    }

    return (
        <Animatable.View 
        style={styles.wrapper}
        duration={1000}
        animation="swing"
        // animation="pulse"
        easing="ease-in-sine"
        // easing="ease-out" 
        // iterationCount="infinite"
        direction='alternate'>
            <Text style={{
                fontSize: 17,
                width: '90%',
                color: colors.merah,
                fontWeight: 'bold'
            }}
                ellipsizeMode="tail"
                numberOfLines={3}
            >{props.name}</Text>
            <Text style={styles.facility}>{`Description: ${props.description}`}</Text>
            <Text style={styles.facility}>{`date: ${moment(props.time).utc().format('YYYY-MM-DD HH:mm')}`}</Text>
            <View style={{ flexDirection: 'row' }}>
                <Text style={styles.facility}>facility / </Text>
                {props.isProjector ? <Text style={styles.facility}>Projector / </Text> : null}
                {props.isHdmi ? <Text style={styles.facility}>HDMI / </Text> : null}
                {props.isBoard ? <Text style={styles.facility}>Board / </Text> : null}
                {props.isConsumption ? <Text style={styles.facility}>{`Consumption ${props.consumption} `}</Text> : null}
            </View>
            <TouchableOpacity
                style={styles.close}
                onPress={props.pressDelete}
            >
                <Icon name='ios-close-circle' size={40} color={colors.merah} />
            </TouchableOpacity>
        </Animatable.View>
    )
}

export default MyBookingsCard

const styles = StyleSheet.create({
    wrapper: {
        marginTop: 10,
        alignContent: 'center',
        borderRadius: 10,
        backgroundColor: 'white',
        elevation: 8,
        padding: 10,
        marginHorizontal: 10,
    },
    wrapperText: {
        justifyContent: 'space-between',
        position: 'absolute',
        bottom: 5,
        left: 5,
        backgroundColor: '#FFFFFFCC',
        width: width * 0.9 - 10
    },
    facility: {
        fontSize: 12,
        color: 'black',
        // fontWeight:'bold'
    },
    close: {
        position: 'absolute',
        top: 5,
        right: 5,
    }
})