/* eslint-disable no-alert */
import React, { useState } from 'react';
import { Dimensions, StyleSheet, Text, TouchableOpacity, View, TouchableWithoutFeedback, Platform } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { colors } from '../utils/colors';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import { Button, CheckBoxComp } from '../components/atoms/';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Picker } from '@react-native-picker/picker';
import queryBookMeetingRoom from '../Api/bookMeetingRoomApi';
import { connect } from 'react-redux';
import fetchBookingRoom from '../stores/actions/bookingRoomAction';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { ScrollView } from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
const { width, height } = Dimensions.get('screen');

const BookingRoomCard = (props) => {
    const [description, setDescription] = useState(' ');
    const [date, setDate] = useState(moment(new Date()).format('YYYY-MM-DD'));
    const [pickTime, setPickTime] = useState('08:00:00');
    const [show, setShow] = useState(false);
    const [isProjector, setIsProjector] = useState(false);
    const [isHDMI, setIsHDMI] = useState(false);
    const [isBoard, setIsBoard] = useState(false);
    const [isConsumption, setIsConsumption] = useState(false);
    const [consumption, setConsumption] = useState('0');


    const sendBook = async (token, room_id, time, description, is_projector, is_hdmi, is_board, is_consumption, consumption) => {
        try {
            await queryBookMeetingRoom(token, room_id, time, description, is_projector, is_hdmi, is_board, is_consumption, consumption)
                .then(async (response) => {
                    // console.warn(response);
                    if (response.data == null) {
                        alert(response.data.message);
                    } else {
                        alert(response.data.message);
                    }
                });
        } catch (error) {
            alert(error.response.data.message);
        }
    };

    const showDatePicker = () => {
        setShow(true);
    };

    const hideDatePicker = () => {
        setShow(false);

    };

    const handleConfirm = (item) => {
        // console.log('A date has been picked: ', item);
        setDate(moment(item).format('YYYY-MM-DD'));
        hideDatePicker();
    };


    return (
        <TouchableWithoutFeedback onPress={props.backPress}>
            <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} style={styles.container}>
                <TouchableOpacity
                    style={styles.buttonBack}
                    onPress={props.backPress}>
                    <Icon name="arrow-back-outline" size={40} color={'#FFFFFF'} />
                </TouchableOpacity>



                <TouchableWithoutFeedback onPress={() => null}>
                    <View style={styles.wrapperBooking}>
                        <Text style={styles.text}>Description</Text>
                        <View style={styles.input2}>
                            <TextInput
                                // style={{backgroundColor:'red'}}
                                onChangeText={(value) => setDescription(value)}
                                value={description}
                                placeholder={'Write your needs'}
                                placeholderTextColor="#979797"
                                // textAlignVertical="top"
                                multiline={true}
                                onSubmitEditing={() => {
                                    null;
                                }}
                            />
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ alignItems: 'center' }}>
                                <Text style={styles.text}>Date</Text>

                                <TouchableOpacity
                                    style={styles.inputDate}
                                    onPress={showDatePicker}
                                >
                                    <Text style={{ marginLeft: 1 }} onPress={showDatePicker}>{moment(date).format('YYYY-MM-DD')}</Text>
                                    <DateTimePickerModal
                                        isVisible={show}
                                        mode="date"
                                        onConfirm={handleConfirm}
                                        onCancel={hideDatePicker}
                                    />
                                    <Icon style={{ marginLeft: 1 }} name="calendar" onPress={showDatePicker} size={25} color={'#696969'} />
                                </TouchableOpacity>
                                <Text style={styles.text}>Time</Text>
                                <View style={styles.inputTime}>
                                    <Picker
                                        selectedValue={pickTime}
                                        style={{
                                            overflow: 'hidden',
                                            flex: 0.55,
                                            paddingHorizontal: 15,
                                            width: '100%',
                                            height: 40,
                                            borderRadius: 25,
                                            backgroundColor: '#D3D3D3',
                                            justifyContent: 'center'
                                        }}
                                        textStyle={{ fontSize: 10, color: 'yellow' }}
                                        onValueChange={(itemValue) => {
                                            setPickTime(itemValue);
                                            // console.log(itemValue)
                                        }
                                        }
                                        itemStyle={{ fontSize: 12 }}
                                    >
                                        <Picker.Item label="08-09" value={'08:00:00'} />
                                        <Picker.Item label="09-10" value={'09:00:00'} />
                                        <Picker.Item label="10-11" value={'10:00:00'} />
                                        <Picker.Item label="11-12" value={'11:00:00'} />
                                        <Picker.Item label="12-13" value={'12:00:00'} />
                                        <Picker.Item label="13-14" value={'13:00:00'} />
                                        <Picker.Item label="14-15" value={'14:00:00'} />
                                        <Picker.Item label="15-16" value={'15:00:00'} />
                                        <Picker.Item label="16-17" value={'16:00:00'} />
                                    </Picker>
                                </View>
                            </View>
                            <View style={{ alignItems: 'center' }}>
                                <Text style={styles.text}>Facility</Text>
                                <View style={styles.inputFasilitas}>
                                    <CheckBoxComp
                                        onPress={() => { setIsProjector(!isProjector); }}
                                        checked={isProjector}
                                        text="Projector"
                                    />
                                    <CheckBoxComp
                                        onPress={() => { setIsHDMI(!isHDMI); }}
                                        checked={isHDMI}
                                        text="HDMI Cable"
                                    />
                                    <CheckBoxComp
                                        onPress={() => { setIsBoard(!isBoard); }}
                                        checked={isBoard}
                                        text="Board"
                                    />
                                    <View style={{ flexDirection: 'row' }}>
                                        <CheckBoxComp
                                            onPress={() => { setIsConsumption(!isConsumption); }}
                                            checked={isConsumption}
                                            text="Consumption"
                                        />
                                        <TextInput
                                            style={{ backgroundColor: 'white', height: 40, marginLeft: 2, textAlign: 'center' }}
                                            onChangeText={(value) => setConsumption(value)}
                                            value={consumption}
                                            placeholder={'pax'}
                                            placeholderTextColor="#979797"
                                            textAlignVertical="top"
                                            multiline={true}
                                            onSubmitEditing={() => {
                                                null;
                                            }}
                                        />
                                    </View>
                                </View>
                            </View>
                        </View>
                        <Button
                            title={'Book Room'}
                            onPress={async () => {
                                const room_id = props.id;
                                const time = `${date} ${pickTime}`;
                                const token = await AsyncStorage.getItem('@token');
                                await sendBook(token, room_id, time, description, isProjector, isHDMI, isBoard, isConsumption, consumption);
                                await props.dispatchBookingRoom(token);
                                props.backPress;
                            }}
                            type={'default'}
                            white={false}
                        />
                    </View>




                    {/* {show && (
                <DateTimePicker
                    testID="dateTimePicker"
                    style={{ width: '100%' }}
                    value={new Date()}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={pickDate}
                />
            )} */}
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        </TouchableWithoutFeedback >
    );
};


function mapStateToProps(state) {
    return {
        bookingRoomStore: state.bookingRoomStore
    };
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchBookingRoom: (token) => dispatch(fetchBookingRoom(token))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(BookingRoomCard);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        backgroundColor: '#AAAAAAdd',
        width: width,
        height: height,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 4
    },
    buttonBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: 50,
        height: 50,
        backgroundColor: '#000000',
        borderRadius: 25,
        left: 20,
        top: 40
    },
    wrapperAtas: {
        width: '100%',
        height: 80,
        backgroundColor: colors.merah,
        justifyContent: 'center',
        borderBottomEndRadius: 20,
        borderBottomStartRadius: 20
    },
    text: {
        fontWeight: 'bold',
        fontSize: 17,
        textAlign: 'center'
    },
    input2: {
        // paddingHorizontal: 10,
        justifyContent: 'center',
        margin: 14,
        width: width * 0.9,
        // height: 60,
        borderRadius: 10,
        backgroundColor: '#dddddd'
    },
    inputDate: {
        marginVertical: 14,
        width: width * 0.4,
        paddingHorizontal: 10,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    inputTime: {
        margin: 14,
        width: width * 0.4,
        paddingHorizontal: 15,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        justifyContent: 'center'
    },
    inputFasilitas: {
        width: width * 0.5,
        marginVertical: 14,
        marginRight: 10,
        padding: 5,
        height: 180,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        alignItems: 'flex-start'
    },
    wrapperBooking: {
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'space-between',
        margin: 10,
        height: height * 0.55,
        backgroundColor: 'white',
        borderRadius: 30,
        paddingVertical: 20
    }
});
