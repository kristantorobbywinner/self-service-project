import { Dimensions, StyleSheet, Text, View, TouchableOpacity, Alert } from 'react-native'
import * as Animatable from 'react-native-animatable';
import React from 'react'
import moment from 'moment';
const { width, height } = Dimensions.get('screen');

const HistoryAttendanceCard = (props) => {
    const dataCard = props.data
    return (
        <Animatable.View
            duration={1000}
            animation="lightSpeedIn"
            // animation="pulse"
            easing="ease-in-sine"
            // easing="ease-out" 
            // iterationCount="infinite"
            direction='alternate'
        >
            <View
                style={styles.userCard}>
                <View style={{ width: '95%', flexDirection: 'row', justifyContent: 'space-between', margin: 10, paddingHorizontal: 10 }}>
                    <Text style={{ color: "#A82929", fontWeight: 'bold', fontSize: 18 }}>{dataCard.date}</Text>
                    <Text style={{ color: "#A82929", fontWeight: 'bold', fontSize: 18 }}>{new Date(props.duration * 1000).toISOString().substr(11, 8)}</Text>
                </View>
                <View style={{ width: '95%', flexDirection: 'row' }}>
                    <Text style={{ width:'25%', color: '#000000', paddingLeft: 10, marginBottom: 10, fontSize: 16, fontWeight: 'bold' }}>Clock In</Text>
                    <Text style={{ width:'75%', color: '#000000', paddingLeft: 10, marginBottom: 10, fontSize: 16 }}>{`${moment(props.clockin).utcOffset('+00:00').format('MMMM Do YYYY, h:mm:ss a')}`}</Text>
                </View>
                <View style={{ width: '95%', flexDirection: 'row' }}>
                    <Text style={{ width:'25%', color: '#000000', paddingLeft: 10, marginBottom: 10, fontSize: 16, fontWeight: 'bold' }}>Clock Out</Text>
                    <Text style={{ width:'75%', color: '#000000', paddingLeft: 10, marginBottom: 10, fontSize: 16 }}>{`${props.clockout ? moment(props.clockout).utcOffset('+00:00').format('MMMM Do YYYY, h:mm:ss a') : null}`}</Text>
                </View>
            </View>
        </Animatable.View>
    )
}

export default HistoryAttendanceCard

const styles = StyleSheet.create({
    userCard: {
        backgroundColor: 'white',
        paddingVertical: 6,
        paddingHorizontal: 6,
        borderRadius: 10,
        marginTop: 10,
        display: 'flex',
        alignItems: 'center',
        width: '100%'
    },
})
