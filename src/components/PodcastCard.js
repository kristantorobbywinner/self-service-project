import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity } from 'react-native';
import { useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons'
import moment from 'moment';
import { colors } from '../utils';
const { width, height } = Dimensions.get('screen');

const PodcastCard = (props) => {
    const navigation = useNavigation();

    return (
        <TouchableOpacity
            onPress={() => navigation.navigate("DetailPodcastPage", { id:props.index,url:props.url,title:props.title,artwork:props.artwork,duration:props.duration })}
        >
            <View style={styles.wrapper}>
                <Image
                    source={{ uri: props.artwork }}
                    style={styles.imageStyle} />
                <View style={styles.wrapperText}>
                    <Text style={{
                        fontSize: 17,
                        width: '90%',
                        color: 'black',
                        fontWeight: 'bold'
                    }}
                        ellipsizeMode="tail"
                        numberOfLines={3}
                    >{props.title}</Text>
                    <Text style={{ fontSize: 12, color: 'black' }}>{props.description}</Text> 
                    <Text style={{ fontSize: 12, color: 'black' }}>{`duration: ${props.duration}`}</Text>
                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon name='md-calendar' size={15} color={colors.merah} />
                        <Text style={{ fontSize: 12, color: 'black', marginLeft: 5 }}>{moment(props.published_date, "YYYYMMDD").fromNow()}</Text>

                    </View>
                    {/* </View> */}
                </View>

                {/* <Text
                    style={{ fontSize: 13, color: 'black', padding: 5, fontWeight: 'bold' }}
                    ellipsizeMode="tail"
                    numberOfLines={3}
                >
                    {props.description}
                </Text> */}


            </View>

        </TouchableOpacity>
    )
}

export default PodcastCard

const styles = StyleSheet.create({
    wrapper: {
        marginTop:10,
        marginHorizontal: width*0.04,
        marginRight:width*0.06,
        marginBottom: 0,
        alignContent: 'center',
        borderColor: colors.merah,
        borderWidth: 3,
        borderRadius: 10,
        backgroundColor: 'white',
        elevation: 8,
        padding: 4,
        width:width*0.9


    },
    imageStyle: {
        width: "95%",
        height: 200,
        margin: 5,
        borderRadius: 10
    },
    wrapperText:{ 
        justifyContent: 'space-between', 
        position:'absolute', 
        bottom:5,
        left:5,
        backgroundColor:'#FFFFFFCC',
        width:width*0.9-10
    }

})