import React, { useState } from 'react'
import changePassword from '../Api/changePasswordApi'
import doLogin from '../Api/loginApi'
import { StyleSheet, Text, View, TouchableOpacity, TouchableWithoutFeedback } from 'react-native'
import Input from './atoms/Input'
import Icon from 'react-native-vector-icons'
import { colors } from '../utils/colors/'
import AsyncStorage from '@react-native-async-storage/async-storage'

const PasswordCard = ({ backPress }) => {
    const [oldPassword, setOldPassword] = useState('')
    const [newPassword, setNewPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [notShownOldPassword, setNotShownOldPassword] = useState(true)
    const [notShownNewPassword, setNotShownNewPassword] = useState(true)
    const [notShownConfirmPassword, setNotShownConfirmPassword] = useState(true)


    const changePasswordOnPressed = async (oldPassword, newPassword, confirmPassword) => {
        try {
            const token = await AsyncStorage.getItem('@token')

            const email = await AsyncStorage.getItem('@email')

            await changePassword(token, oldPassword, newPassword, confirmPassword)
                .then((response) => {
                    if (response.data.code == 200) {
                        reLogin(email, newPassword)
                        alert('Change Password Succeeded')
                        backPress()
                    } else {
                        alert('Change Password Failed')
                    }
                })
        } catch (error) {
            alert('Change Password Failed')
        }
    }

    const reLogin = async (email, password) => {
        try {
            await doLogin(email, password)
                .then((response) => {
                    setNewToken(response.data.data.token)
                })
        } catch (error) {
            throw error
        }
    }

    const setNewToken = async (token) => {
        try {
            await AsyncStorage.setItem('@token', token)
        } catch (error) {
            throw error
        }
    }

    return (
        <TouchableWithoutFeedback onPress={backPress}>
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={() => { }}>
                    <View style={styles.wrapper}>
                        <View style={styles.formContainer}>
                            <Input
                                placeholder={'Old Password'}
                                value={oldPassword}
                                onChangeText={(value) => setOldPassword(value)}
                                notShowPassword={notShownOldPassword}
                                onPress={() => setNotShownOldPassword(!notShownOldPassword)}
                                type={'password'} />
                        </View>

                        <View style={styles.formContainer}>
                            <Input
                                placeholder={'New Password'}
                                value={newPassword}
                                onChangeText={(value) => setNewPassword(value)}
                                notShowPassword={notShownNewPassword}
                                onPress={() => setNotShownNewPassword(!notShownNewPassword)}
                                type={'password'} />
                        </View>

                        <View style={styles.formContainer}>
                            <Input
                                placeholder={'New Password'}
                                value={confirmPassword}
                                onChangeText={(value) => setConfirmPassword(value)}
                                notShowPassword={notShownConfirmPassword}
                                onPress={() => setNotShownConfirmPassword(!notShownConfirmPassword)}
                                type={'password'} />
                        </View>


                        <TouchableOpacity style={styles.submitButton}
                            onPress={() =>
                                changePasswordOnPressed(oldPassword, newPassword, confirmPassword)}>
                            <Text style={{ color: colors.putih }}>Change Password</Text>
                        </TouchableOpacity>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        </TouchableWithoutFeedback>
    )
}

export default PasswordCard

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        backgroundColor: '#000000bc',
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    wrapper: {
        borderRadius: 20,
        padding: 20,
        width: '80%',
        marginVertical: 40,
        alignItems: 'center',
        backgroundColor: colors.abuabumuda,
    },
    formContainer: {
        marginTop: 20,
        marginHorizontal: 20,
        width: '100%',
        padding: 2,
        borderRadius: 20,
        backgroundColor: colors.putih,
    },
    submitButton: {
        padding: 10,
        width: '80%',
        marginTop: 20,
        borderRadius: 40,
        alignItems: 'center',
        backgroundColor: colors.abuabutua,
    },
})