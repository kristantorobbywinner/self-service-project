import React, { useEffect, useState } from 'react'
import { StyleSheet, KeyboardAvoidingView, View, Text, TextInput, Touchable, TouchableOpacity, TouchableHighlight, TouchableWithoutFeedback, Platform, Keyboard } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { colors } from '../utils'
import DateTimePicker from '@react-native-community/datetimepicker'
import moment from 'moment'
import editFamilyRelation from '../Api/editFamilyRelationApi'
import AsyncStorage from '@react-native-async-storage/async-storage'
import DateTimePickerModal from "react-native-modal-datetime-picker"
import addFamilyRelation from '../Api/addFamilyRelationApi'
import { Picker } from '@react-native-picker/picker'


const EditFamilyRelationCard = ({ selectedMode, relationStatus, relationName, dateOfBirth, address, backPress }) => {
    const [newName, setNewName] = useState(relationName)
    const [newDateOfBirth, setNewDateOfBirth] = useState(dateOfBirth)
    const [newAddress, setNewAddress] = useState(address)
    const [relation, setRelation] = useState(relationStatus)
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);

    useEffect(() => {
        setRelationToAPI(relationStatus)
        console.log(selectedMode)
    }, [])


    const setRelationToAPI = (relationStatus) => {
        switch (relationStatus) {
            case 'Father':
                setRelation('ayah');
                break;
            case 'Mother':
                setRelation('ibu');
                break;
            case 'Spouse':
                setRelation('spouse');
                break;
            case 'First Child':
                setRelation('anak ke 1');
                break;
            case 'Second Child':
                setRelation('anak ke 2');
                break;
            default:
                break;
        }
    }

    const submitPressed = async (mode) => {

        switch (mode) {
            case 'edit':
                try {
                    const token = await AsyncStorage.getItem('@token')

                    await editFamilyRelation(token, newName, relation, newDateOfBirth, newAddress)
                        .then((response) => {
                            alert('Successful')
                        }).catch((error) => {
                            alert('Please Try Again')
                        })

                } catch (error) {
                    throw error
                }
                break;
            case 'add':
                try {
                    const token = await AsyncStorage.getItem('@token')

                    await addFamilyRelation(token, newName, relation, newDateOfBirth, newAddress)
                        .then((response) => {
                            alert('Successful')
                        }).catch((error) => {
                            alert('Please Try Again')
                        })
                } catch (error) {
                    throw error
                }
            default:
                break;
        }

    }

    useEffect(() => {
        console.log(dateOfBirth)
    }, [])

    return (
            <TouchableWithoutFeedback onPress={backPress}>
                <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'} style={styles.container}>
                    <TouchableWithoutFeedback onPress={()=> {Keyboard.dismiss}}>
                        <View style={styles.wrapper}>
                            <Text>{relationStatus}'s Information</Text>

                            {(relationStatus == 'Parent') ?
                                <View style={styles.formContainer}>
                                    <Text style={styles.formTitle}>Add Relation</Text>
                                    {/* <TextInput
                                    placeholder='Name'
                                    value={newName}
                                    autoCorrect={false}
                                    multiline={true}
                                    onChangeText={(value) => {
                                        setNewName(value)
                                        setShow(false)
                                    }}
                                    style={styles.form} /> */}

                                    <View
                                        style={{
                                            padding: 10,
                                            textAlignVertical: 'top',
                                            width: '100%',
                                            height: 80,
                                            borderRadius: 20,
                                        }}>
                                        <Picker
                                            selectedValue={relation}
                                            style={{
                                                overflow: 'hidden',
                                                flex: 0.55,
                                                paddingHorizontal: 15,
                                                height: 40,
                                                borderRadius: 25,
                                                backgroundColor: '#D3D3D3',
                                                justifyContent: 'center',
                                            }}
                                            textStyle={{ fontSize: 12 }}
                                            onValueChange={(itemValue, itemIndex) => {
                                                setRelation(itemValue);
                                            }}
                                            itemStyle={{ fontSize: 15 }}>
                                            <Picker.Item label="Father" value={'ayah'} />
                                            <Picker.Item label="Mother" value={'ibu'} />
                                        </Picker>
                                    </View>


                                </View>
                                : null}

                            <View style={styles.formContainer}>
                                <Text style={styles.formTitle}>Name</Text>
                                <TextInput
                                    placeholder='Name'
                                    value={newName}
                                    autoCorrect={false}
                                    multiline={true}
                                    onChangeText={(value) => {
                                        setNewName(value)
                                        setShow(false)
                                    }}
                                    style={styles.form} />
                            </View>

                            <View style={styles.formContainer}>
                                <Text style={styles.formTitle}>Date Of Birth</Text>
                                <View></View>
                            </View>

                            <TouchableOpacity style={styles.dateContainer}
                                onPress={() => {
                                    setShow(true)
                                }}>
                                <DateTimePickerModal
                                    isVisible={show}
                                    mode='date'
                                    onConfirm={(item) => {
                                        setNewDateOfBirth(item)
                                        setShow(false)
                                    }}
                                    onCancel={() => {
                                        setShow(false)
                                    }}
                                />
                                <Text
                                    style={{
                                        flex: 6,
                                        padding: 10,
                                        overflow: 'hidden',
                                        height: 40,
                                        marginLeft: 10,
                                    }}>
                                    {moment(newDateOfBirth).format('DD MMMM YYYY')}
                                </Text>

                                <Icon name={'ios-calendar'} style={{ flex: 1 }} size={20} color={colors.abuabutua} />

                            </TouchableOpacity>

                            <View style={styles.formContainer}>
                                <Text style={styles.formTitle}>Alamat</Text>
                                <TextInput
                                    placeholder='Address'
                                    value={newAddress}
                                    autoCorrect={false}
                                    multiline={true}
                                    onChangeText={(value) => {
                                        setNewAddress(value)
                                        //setShow(false)                           
                                    }}
                                    style={styles.addressForm} />
                            </View>

                            <TouchableOpacity
                                style={styles.submitButton}
                                onPress={async () => {
                                    await submitPressed(selectedMode)
                                    backPress()
                                }}>
                                <Text style={{ color: colors.putih }}>Save</Text>
                            </TouchableOpacity>
                        </View>
                    </TouchableWithoutFeedback>
                    
                </KeyboardAvoidingView>
            </TouchableWithoutFeedback>
    )
}

export default EditFamilyRelationCard

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        backgroundColor: '#000000bc',
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
    },
    wrapper: {
        borderRadius: 20,
        padding: 20,
        width: '80%',
        alignItems: 'center',
        backgroundColor: colors.abuabumuda,
    },
    dateContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 25,
        backgroundColor: colors.putih,
    },
    formContainer: {
        marginTop: 20,
        width: '100%',
        padding: 2,
    },
    formTitle: {
        paddingTop: 10,
        paddingBottom: 10,
    },
    form: {
        padding: 10,
        textAlignVertical: 'top',
        width: '100%',
        height: 40,
        borderRadius: 20,
        backgroundColor: colors.putih,
    },
    addressForm: {
        padding: 10,
        textAlignVertical: 'top',
        width: '100%',
        height: 80,
        borderRadius: 20,
        backgroundColor: colors.putih,
    },
    submitButton: {
        padding: 10,
        width: '80%',
        marginTop: 20,
        borderRadius: 40,
        alignItems: 'center',
        backgroundColor: colors.abuabutua,
    }
})