import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity } from 'react-native';
import { useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons'
import moment from 'moment';
import { colors } from '../utils';
const { width, height } = Dimensions.get('screen');

const NewsCard = (props) => {
    const navigation = useNavigation();

    return (
        <TouchableOpacity
            onPress={() => navigation.navigate("DisplayWeb", { url: props.url, title: props.title })}
        >
            <View style={styles.wrapper}>
                <Image
                    source={{ uri: props.imageMC }}
                    style={styles.imageStyle} />
                <View style={styles.wrapperText}>
                    <Text style={{
                        fontSize: 17,
                        width: '90%',
                        color: 'black',
                        fontWeight: 'bold'
                    }}
                        ellipsizeMode="tail"
                        numberOfLines={3}
                    >{props.title}</Text>
                    <Text style={{ fontSize: 12, color: 'black' }}>{props.source}</Text>
                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <Icon name='md-calendar' size={15} color={colors.merah} />
                        <Text style={{ fontSize: 12, color: 'black', marginLeft: 5 }}>{moment(props.publishedAt, "YYYYMMDD").fromNow()}</Text>

                    </View>
                    {/* </View> */}
                </View>
            </View>

        </TouchableOpacity>
    )
}

export default NewsCard

const styles = StyleSheet.create({
    wrapper: {
        marginTop:10,
        marginHorizontal: width*0.04,
        marginBottom: 0,
        alignContent: 'center',
        borderColor: colors.merah,
        borderWidth: 3,
        borderRadius: 10,
        backgroundColor: 'white',
        elevation: 8,
        padding: 5,
        width:width*0.9


    },
    imageStyle: {
        width: "100%",
        height: 200,
        borderRadius: 10,
    },
    wrapperText:{ 
        justifyContent: 'space-between', 
        position:'absolute', 
        bottom:5,
        left:5,
        backgroundColor:'#FFFFFFCC',
        width:'100%',
    }

})