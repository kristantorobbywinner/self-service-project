/* eslint-disable no-alert */
import React, { useState } from 'react';
import { Dimensions, StyleSheet, Text, TouchableOpacity, View, TouchableWithoutFeedback } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { colors } from '../utils/colors';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import { Button } from './atoms';
import AsyncStorage from '@react-native-async-storage/async-storage';
import editNote from '../Api/editNoteApi';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

const { width, height } = Dimensions.get('screen');

const VersionUpdateCard = (props) => {

    return (
        <TouchableOpacity
            style={styles.container}
            onPress={props.backPress}
        >
            {/* <TouchableOpacity
                style={styles.buttonBack}
                onPress={props.backPress}>
                <Icon name="arrow-back-outline" size={40} color={'#FFFFFF'} />
            </TouchableOpacity> */}
            <TouchableWithoutFeedback onPress={() => null}>
                <View style={styles.wrapperBooking}>
                    <Text style={{fontWeight:'bold', fontSize:20}}>Software Update</Text>
                    <Text style={{fontWeight:'bold', fontSize:18}}>Your current software version : </Text>
                    <Text style={{fontWeight:'bold', fontSize:15}}>{props.curVer}</Text>
                    <Text style={{fontWeight:'bold', fontSize:18}}>Please update to version : </Text>
                    <Text style={{fontWeight:'bold', fontSize:15}}>{props.version.version}</Text>

                    <Button
                    title={'update'}
                    onPress={()=>{
                        alert(`go to ${props.version.download_url}`)
                    }}
                    type={'default'}
                    white={false}
                    />
                </View>
            </TouchableWithoutFeedback>
        </TouchableOpacity>
    );
};

export default VersionUpdateCard;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        backgroundColor: '#AAAAAAdd',
        width: width,
        height: height,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 10
    },
    wrapperBooking: {
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'space-between',
        margin: 10,
        height: height * 0.35,
        backgroundColor: colors.abuabumuda,
        borderRadius: 30,
        paddingVertical: 20,
        width: '90%'
    }
});
