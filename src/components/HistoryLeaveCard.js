import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import moment from 'moment';


const HistoryLeaveCard = ({ typeLeave, startDateLeave, endDateLeave, durationLeave }) => {
    return (
        <View style={styles.wrapper}>
            <View style={{ flex: 0.7, flexDirection: 'column' }}>
                <Text style={{ fontWeight: 'bold', color: '#000000', fontSize: 20 }}>{typeLeave}</Text>
                <Text style={{ fontWeight: 'bold', color: '#A82929', fontSize: 15 }}>
                    {moment(startDateLeave).format('DD MMM YYYY')} - {moment(endDateLeave).format('DD MMM YYYY')}
                </Text>
            </View>
            <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'flex-end' }}>
                <Text style={{ fontWeight: 'bold', color: '#A82929', fontSize: 25 }}>{durationLeave} Days</Text>
            </View>
        </View>
    );
};

export default HistoryLeaveCard;

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 25,
        marginHorizontal: 20,
        marginVertical: 3,
        backgroundColor: '#FFFFFF',
        paddingVertical: '5%',
        paddingHorizontal: '7%'

    }
});
