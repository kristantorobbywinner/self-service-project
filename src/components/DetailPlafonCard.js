import React from 'react';
import { StyleSheet, Text, View, Dimensions, TouchableWithoutFeedback, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const { width, height } = Dimensions.get('window');

const currencyFormat = amount => {
    return Number(amount)
        .toFixed(2)
        .replace(/\d(?=(\d{3})+\.)/g, '$&,');
};

const DetailPlafonCard = ({ balance, backPress }) => {
    return (
        <TouchableWithoutFeedback onPress={backPress}>
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.buttonBack}
                    onPress={backPress}>
                    <Icon name="arrow-back-outline" size={40} color={'#FFFFFF'} />
                </TouchableOpacity>
                <TouchableWithoutFeedback onPress={() => { }}>
                    <View style={styles.wrapper}>
                        <Text style={{ fontSize: 20, marginBottom: 10 }}>Detail Plafond</Text>
                        <View style={{ width: 0.7 * width, flexDirection: 'row' }}>
                            <View style={{ width: '30%' }}>
                                <Text style={{ fontSize: 15, color: '#000000', textAlign: 'center' }}>Type</Text>
                            </View>
                            <View style={{ width: '35%' }}>
                                <Text style={{ fontSize: 15, color: '#000000', fontWeight: 'bold', textAlign: 'center' }}>Terpakai</Text>
                            </View>
                            <View style={{ width: '35%' }}>
                                <Text style={{ fontSize: 15, color: '#000000', textAlign: 'left' }}>/ Plafond</Text>
                            </View>
                        </View>

                        <FlatList
                            data={balance}
                            renderItem={({ item, index }) => {
                                return (
                                    <View style={styles.listWrapper}>
                                        <View style={{ width: '30%' }}>
                                            <Text style={{ fontSize: 12, color: '#FFFFFF', textAlign: 'center' }}>{item.benefit.type_name} </Text>
                                        </View>
                                        <View style={{ width: '35%' }}>
                                            <Text style={{ fontSize: 12, color: '#FFFFFF', fontWeight: 'bold' }}>Rp. {currencyFormat(item.type_usage)} </Text>
                                        </View>
                                        <View style={{ width: '35%' }}>
                                            <Text style={{ fontSize: 12, color: '#FFFFFF' }}>/ {currencyFormat(item.benefit.type_limit)} </Text>
                                        </View>
                                    </View>
                                );
                            }}
                            keyExtractor={(item) => item.type.toString()}
                        />
                    </View>
                </TouchableWithoutFeedback>
            </View>
        </TouchableWithoutFeedback>
    );
};

export default DetailPlafonCard;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        backgroundColor: '#00000099',
        width: width,
        height: height,
        alignItems: 'center',
        justifyContent: 'center'
    },
    wrapper: {
        alignItems: 'center',
        justifyContent: 'center',
        width: width - 40,
        backgroundColor: '#FFFFFF',
        borderRadius: 30,
        paddingVertical: 20
    },
    buttonBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: 50,
        height: 50,
        backgroundColor: '#000000',
        borderRadius: 25,
        left: 20,
        top: 20
    },
    listWrapper: {
        width: 0.7 * width,
        flexDirection: 'row',
        backgroundColor: '#A82929',
        marginVertical: 5,
        paddingVertical: 10,
        borderRadius: 25
    }

});
