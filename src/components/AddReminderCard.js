/* eslint-disable no-alert */
import React, { useState } from 'react';
import { Dimensions, StyleSheet, Text, TouchableOpacity, View, TouchableWithoutFeedback, Platform } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { colors } from '../utils/colors';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';
import { Button } from '../components/atoms/';
import AsyncStorage from '@react-native-async-storage/async-storage';
import addReminder from '../Api/addReminderApi';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

const { width, height } = Dimensions.get('screen');

const AddReminderCard = (props) => {
    const [description, setDescription] = useState(' ');
    const [startDate, setStartDate] = useState(moment(new Date()).format('YYYY-MM-DD HH:mm'));
    const [startTime, setStartTime] = useState(moment(new Date()).format('YYYY-MM-DD HH:mm'));
    const [endDate, setEndDate] = useState(moment(new Date()).format('YYYY-MM-DD HH:mm'));
    const [endTime, setEndTime] = useState(moment(new Date()).format('YYYY-MM-DD HH:mm'));
    const [mode, setMode] = useState('');
    const [show, setShow] = useState(false);
    const [showTime, setShowTime] = useState(false);
    const [isStart, setIsStart] = useState(true);

    const pickDate = (event, selectedDate) => {
        const currentDate = selectedDate || startDate;
        setShow(Platform.OS === 'ios');

        if (isStart) {
            setStartDate(moment(currentDate).format('YYYY-MM-DD HH:mm'));
        }
        else {
            setEndDate(moment(currentDate).format('YYYY-MM-DD HH:mm'));
        }
    };

    const pickTime = (event, selectedDate) => {
        const currentDate = selectedDate || startDate;
        setShowTime(Platform.OS === 'ios');

        if (isStart) {
            setStartTime(moment(currentDate).format('YYYY-MM-DD HH:mm'));
        }
        else {
            setEndTime(moment(currentDate).format('YYYY-MM-DD HH:mm'));
        }
    };


    const sendAddReminder = async (token, time_start, time_stop, description) => {
        try {
            await addReminder(token, time_start, time_stop, description)
                .then(async (response) => {
                    // console.warn(response);
                    if (response.data == null) {
                        alert('submit failed');
                    } else {
                        alert('submit berhasil');
                    }
                });
        } catch (error) {
            alert(error);
        }
    };

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
        // console.log(date);
    };
    const showTimeMode = (currentMode) => {
        setShowTime(true);
        setMode(currentMode);
        // console.log(date);
    };

    const showStartDatepicker = () => {
        showMode('date');
        setIsStart(true);
    };

    const showStartTimepicker = () => {
        showTimeMode('time');
        setIsStart(true);
    };

    const showEndDatepicker = () => {
        showMode('date');
        setIsStart(false);
    };

    const showEndTimepicker = () => {
        showTimeMode('time');
        setIsStart(false);
    };

    // modal time picker
    const showDatePicker = () => {
        setShow(true);
    };
    const showTimePicker = () => {
        setShowTime(true);
    };

    const hideDatePicker = () => {
        setShow(false);
    };
    const hideTimePicker = () => {
        setShowTime(false);
    };

    return (
        <TouchableOpacity
            style={styles.container}
            onPress={props.backPress}
        >
            <TouchableOpacity
                style={styles.buttonBack}
                onPress={props.backPress}>
                <Icon name="arrow-back-outline" size={40} color={'#FFFFFF'} />
            </TouchableOpacity>
            <TouchableWithoutFeedback onPress={() => null}>
                <View style={styles.wrapperBooking}>
                    <Text style={styles.text}>Description</Text>
                    <View style={styles.input2}>
                        <TextInput
                            // style={styles.input2}
                            onChangeText={(value) => setDescription(value)}
                            value={description}
                            placeholder={'Write your needs'}
                            placeholderTextColor="#979797"
                            textAlignVertical="top"
                            multiline={true}
                            onSubmitEditing={() => {
                                null;
                            }}
                        />
                    </View>
                    <Text style={styles.text}>Start :</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'flex-end', width: width * 0.9 }}>
                        <TouchableOpacity
                            style={styles.inputDate}
                            onPress={showDatePicker}
                        >
                            <Text style={{ marginLeft: 1 }} onPress={showDatePicker}>{moment(startDate).format('YYYY-MM-DD')}</Text>
                            <DateTimePickerModal
                                isVisible={show}
                                mode="date"
                                onConfirm={(item) => {
                                    setStartDate(moment(item).format('YYYY-MM-DD'));
                                    hideDatePicker();
                                }}
                                onCancel={hideDatePicker}
                            />
                            <Icon style={{ marginLeft: 1 }} name="calendar" onPress={showDatePicker} size={25} color={'#696969'} />
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.inputDate}
                            onPress={showTimePicker}
                        >
                            <Text style={{ marginLeft: 1 }} onPress={showTimePicker}>{moment(startTime).format('HH:mm')}</Text>
                            <DateTimePickerModal
                                isVisible={showTime}
                                mode="time"
                                onConfirm={(item) => {
                                    setStartTime(item);
                                    hideTimePicker();
                                }}
                                onCancel={hideDatePicker}
                            />
                            <Icon style={{ marginLeft: 1 }} name="time" onPress={showTimePicker} size={25} color={'#696969'} />
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.text}>End :</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'flex-end', width: width * 0.9 }}>
                        <TouchableOpacity
                            style={styles.inputDate}
                            onPress={showDatePicker}
                        >
                            <Text style={{ marginLeft: 1 }} onPress={showDatePicker}>{moment(endDate).format('YYYY-MM-DD')}</Text>
                            <DateTimePickerModal
                                isVisible={show}
                                mode="date"
                                onConfirm={(item) => {
                                    setEndDate(moment(item).format('YYYY-MM-DD'));
                                    hideDatePicker();
                                }}
                                onCancel={hideDatePicker}
                            />
                            <Icon style={{ marginLeft: 1 }} name="calendar" onPress={showDatePicker} size={25} color={'#696969'} />
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.inputDate}
                            onPress={showTimePicker}
                        >
                            <Text style={{ marginLeft: 1 }} onPress={showTimePicker}>{moment(endTime).format('HH:mm')}</Text>
                            <DateTimePickerModal
                                isVisible={showTime}
                                mode="time"
                                onConfirm={(item) => {
                                    setEndTime(item);
                                    hideTimePicker();
                                }}
                                onCancel={hideDatePicker}
                            />
                            <Icon style={{ marginLeft: 1 }} name="time" onPress={showTimePicker} size={25} color={'#696969'} />
                        </TouchableOpacity>
                    </View>
                    <Button
                        title={'Add Reminder'}
                        onPress={async () => {
                            const token = await AsyncStorage.getItem('@token');
                            const time_start = `${moment(startDate).format('YYYY-MM-DD')} ${moment(startTime).format('HH:mm')}`;
                            const time_stop = `${moment(endDate).format('YYYY-MM-DD')} ${moment(endTime).format('HH:mm')}`;
                            await sendAddReminder(token, time_start, time_stop, description);
                            props.backPress;
                        }}
                        type={'default'}
                        white={false}
                    />
                </View>
            </TouchableWithoutFeedback>

            {/* {show && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={new Date()}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={pickDate}
                />
            )}
            {showTime && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={new Date()}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={pickTime}
                />
            )} */}
        </TouchableOpacity>
    );
};

export default AddReminderCard;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        backgroundColor: '#AAAAAAdd',
        width: width,
        height: height,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 10
    },
    buttonBack: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: 50,
        height: 50,
        backgroundColor: '#000000',
        borderRadius: 25,
        left: 20,
        top: 20
    },
    wrapperAtas: {
        width: '100%',
        height: 80,
        backgroundColor: colors.merah,
        justifyContent: 'center',
        borderBottomEndRadius: 20,
        borderBottomStartRadius: 20
    },
    text: {
        fontWeight: 'bold',
        fontSize: 17,
        textAlign: 'center'
    },
    input2: {
        paddingHorizontal: 10,
        margin: 14,
        width: width * 0.9,
        height: 60,
        borderRadius: 10,
        backgroundColor: '#dddddd'
    },
    inputDate: {
        marginVertical: 14,
        width: width * 0.4,
        paddingHorizontal: 10,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    inputTime: {
        margin: 14,
        width: width * 0.4,
        paddingHorizontal: 15,
        height: 40,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        justifyContent: 'center'
    },
    inputFasilitas: {
        width: width * 0.5,
        marginVertical: 14,
        marginRight: 10,
        padding: 5,
        height: 180,
        borderRadius: 25,
        backgroundColor: '#D3D3D3',
        alignItems: 'flex-start'
    },
    wrapperBooking: {
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'space-between',
        margin: 10,
        borderWidth: 4,
        borderColor: colors.merah,
        height: height * 0.55,
        backgroundColor: 'white',
        borderRadius: 30,
        paddingVertical: 20
    }
});
